#!/usr/bin/python3

from distutils.core import setup

setup(name='Freeduc-Usb',
      version='1.9',
      description='Freeduc USB Utilities',
      author='Georges Khaznadar',
      author_email='georgesk@debian.org',
      license='GPL-3+',
      url='https://salsa.debian.org/georgesk/freeduc-usb',
      packages=['freeduc_usb', 'clone_knoppix'],
      package_data={
          'freeduc_usb': ['freeduc-usb/images/*'],
      }
     )
