# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file main.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .Ui_main import Ui_MainWindow
from . import actions, diskimg, sudo

import os.path, subprocess

class mainWindow(QMainWindow):
    """
    just a proxy, adding eventually a few local methods
    Ui_MainWindow is designed with the utility "designer" from qt4-tools.
    Do not modify the file Ui_MainWindow.py by hand. Rather edit main.ui
    with "designer" and then "make all".
    """

    def __init__(self, parent=None, argv=[]):
        """
        The constructor
        @param parent a parent window, None by default
        @param argv command-line arguments
        """
        ######QT
        QMainWindow.__init__(self)
        QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.mountpoints=[]
        # treeSticks and the main application will share the list of mountpoints
        self.ui.treeSticks.mountpoints=self.mountpoints
        self.ui.treeSticks.update()
        self.cloopmountpoints=[]

        ####intialize directories
        self._dir()

        #internal connections
        self.ui_connections()

        # consider command line options
        self.considerOptions(argv)

        self.restoreData()


    def ui_connections(self):
        """
        Connect signals with methods
        """
        QObject.connect(self.ui.action_Quit,SIGNAL("triggered()"), self.close)
        QObject.connect(self.ui.pushDrivesUpdate,SIGNAL("clicked()"), self.ui.treeSticks.update)
        QObject.connect(self.ui.addFileButton,SIGNAL("clicked()"), self.addFileEntry)
        QObject.connect(self.ui.addIsoButton,SIGNAL("clicked()"), self.addIsoEntry)
        QObject.connect(self.ui.addDirectory,SIGNAL("clicked()"), self.addProject)
        QObject.connect(self.ui.tabWidget,SIGNAL("currentChanged(int)"), self.tabChanged)

        # defines a timer to update periodically the tree view
        self.timer1=QTimer()
        self.timer1.setInterval (5000) # five seconds
        self.timer1.start()
        QObject.connect(self.timer1, SIGNAL("timeout()"), self.ui.treeSticks.update)

        # defines the actions for some dropButtons
        self.ui.toolContents.setDropAction(actions.displayPartitionContents)
        self.ui.toolParted.setDropAction(actions.partitionEditor)
        self.ui.toolQemu.setDropAction(actions.partitionQemuLauncher)
        self.ui.bootButton.setDropAction(actions.makeBootable)
        self.ui.toolPartitionInFileContents.setDropAction(actions.displayFilePartitionContents)
        self.ui.rmButton.setDropAction(self.rmFileEntry)
        self.ui.cloneButton.setDropAction(actions.fileBarCloneLauncher)
        self.ui.toolQemuFile.setDropAction(actions.fileQemuLauncher)
        self.ui.isoButton.setDropAction(actions.displayIsoContents)
        self.ui.toolQemuIso.setDropAction(actions.isoQemuLauncher)
        self.ui.cloopButton.setDropAction(actions.displayCloopContents)
        self.ui.chrootButton.setDropAction(actions.displayChrootContents)
        self.ui.toolChroot.setDropAction(actions.chrootToLive)
        return

    def postProcess(self, reply):
        """
        Called after a dropButton event, if this event returns something
        to postprocess.
        @param reply currently MountPoint objects are taken in account
        """
        if type(reply) != type (dict()): return
        if "mountpoint" in reply.keys():
            self.mountpoints.append(reply["mountpoint"])
        elif "cloopmountpoint"  in reply.keys():
            self.cloopmountpoints.append(reply["cloopmountpoint"])

    def considerOptions(self, argv):
        """
        take in account command line options
        @param argv command line options, not previously parsed
        """
        return

    def _dir(which=None):
        """
        @param which a keyword to get some noticeable directory.
        Recognized keywords are: lang,
        @return some noticeable directory
        """
        if which=="lang":
            rep_install= os.path.dirname(os.path.abspath(__file__))
            for dir in (os.path.join(rep_install, 'lang'),
                        '/usr/share/freeduc-usb-manager/lang'):
                if os.path.exists(dir):
                    return dir
            return ""
        return None

    _dir=staticmethod(_dir)

    def addIsoEntry(self):
        """
        Opens a file dialog and adds a file entry into the file tree view
        """
        filename = QFileDialog.getOpenFileName(self, 'Open ISO image file',
                                               '.',
                                               'ISO image files [*.iso] (*.iso);; All files (*)')
        if filename and os.path.exists(filename):
            self.ui.isoListView.addItem(filename)

    def addFileEntry(self):
        """
        Opens a file dialog and adds a file entry into the file tree view
        """
        filename = QFileDialog.getOpenFileName(self, 'Open disk image file',
                                               '.',
                                               'Disk image files [*.img] (*.img);; All files (*)')
        if filename and os.path.exists(filename) and os.path.exists(filename+'.sfdisk'):
            img=diskimg.diskImg("%s" %filename)
            if img.ok:
                self.ui.treeFiles.addEntry(img)

    def addProject(self):
        """
        Opens a file dialog and adds a directory entry into the project tree view
        """
        dirname = QFileDialog.getExistingDirectory (self,
                                                    "Find a new project directory",
                                                    ".")
        dirname=u"%s" %dirname
        if dirname and os.path.exists(dirname) and os.path.exists(os.path.join(dirname,"live")):
            self.ui.projectDirs.addItem(dirname)

    def tabChanged(self, tabNo):
        """
        invoked whenever the current tab is changed
        """
        if tabNo==3: # the Cloop TAB
            #check whether the module cloop is loaded, then try to load it.
            if not self.ensureCloop():
                QMessageBox.critical(self,
                                     "The module 'cloop' is missing",
                                     "You should install the module cloop for this kernel.")
            self.ui.cloopListView.findInMounted(self.mountpoints)

    def ensureCloop(self):
        """
        ensures that the cloop module is available and loaded
        @return a boolean value, True in case of success.
        """
        retcode=subprocess.call("lsmod | grep -q cloop", shell=True)
        if retcode==0:
            return True
        else: # check the availability of the module cloop and activates it
            retcode=sudo.call("modprobe --dry-run cloop", shell=True)
            if retcode==0:
                return True
        return False


    def rmFileEntry(self, data):
        """
        Gets a drop from the file tree view and removes a file entry
        @param event the drop event
        """
        partition,img=data
        self.ui.treeFiles.rmEntry(img)

    def isDirty(self):
        """
        @return True if something which can be saved has been modified,
        False if the saved stated is consistent.
        """
        return self.ui.isoListView.isDirty or self.ui.treeFiles.isDirty or self.ui.projectDirs.isDirty

    def removeCloopMounts(self):
        """
        removes cloop mounts
        """
        for m in self.cloopmountpoints: #unmount every cloop mountpoint
            # print "unmounting and erasing '%s'" %m
            retcode=sudo.call("umount '%s' && sudo rmdir '%s'" %(m.mountpoint,m.mountpoint), shell=True)==0
            if not retcode:
                QMessageBox.critical(self,
                                     "Some temporary directories did remain",
                                     "You should remove manually '%s' as a cloop mountpoint" %m.mountpoint)
        # removing the cloop module should not be necessary
        if subprocess.call("lsmod | grep -q cloop", shell=True) == 0:
            sudo.call("rmmod cloop", shell=True, stderr=None)

    def removeTempMounts(self):
        """
        removes temporary mounts
        """
        for m in self.mountpoints: #unmount every temporary mountpoint
            if m.temporary:
                # print "unmounting and erasing '%s'" %m
                retcode=sudo.call("sudo umount '%s' && sudo rmdir '%s'" %(m.mountpoint,m.mountpoint), shell=True)==0
                if not retcode:
                    QMessageBox.critical(self,
                                         "Some temporary directories did remain",
                                         "You should remove manually '%s'" %m.mountpoint)

    def closeEvent(self,event):
        """
        manages the close event for the main window; if the file tree view
        contains changes, it triggers a message box to ask whether session
        data should be saved. Unmounts the temporary mounts. Cloop mounts are
        remove prior to temporary mounts because they may depend on them.
        """
        self.removeCloopMounts()
        self.removeTempMounts()
        if not self.isDirty():
            event.accept()
            return
        ret = QMessageBox.warning(self,
                                  "About to quit Freeduc-USB",
                                  "Save changes?",
                                  QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        if ret==QMessageBox.Save:
            self.ui.treeFiles.save(self.cfgDir())
            self.ui.isoListView.save(self.cfgDir())
            self.ui.projectDirs.save(self.cfgDir())
            event.accept()
        elif ret==QMessageBox.Discard:
            event.accept()
        elif ret==QMessageBox.Cancel or True: # by default
            event.ignore()
        return

    def restoreData(self):
        """
        restore some data from the configuration directory
        """
        imgFileName=os.path.join(self.cfgDir(),'imgfiles')
        if os.path.exists(imgFileName):
            imgFile=open(imgFileName,"r")
            imgFileNames=imgFile.readlines()
            for f in imgFileNames:
                f=f.strip()
                self.ui.treeFiles.addFile(f)
            self.ui.treeFiles.isDirty=False
            imgFile.close()
        isoFileName=os.path.join(self.cfgDir(),'isofiles')
        if os.path.exists(isoFileName):
            isoFile=open(isoFileName,"r")
            isoFileNames=isoFile.readlines()
            for f in isoFileNames:
                f=f.strip()
                self.ui.isoListView.addItem(f)
            self.ui.isoListView.isDirty=False
            isoFile.close()
        projectName=os.path.join(self.cfgDir(),'projectfiles')
        if os.path.exists(projectName):
            project=open(projectName,"r")
            projectNames=project.readlines()
            for f in projectNames:
                f=f.strip()
                self.ui.projectDirs.addItem(f)
            self.ui.projectDirs.isDirty=False
            project.close()





    def cfgDir(self):
        """
        @return the configuration directory
        """
        dir=os.path.expanduser("~/.freeduc-usb")
        if not os.path.isdir(dir):
            os.mkdir(dir)
        return dir
