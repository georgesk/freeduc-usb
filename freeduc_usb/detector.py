# -*- coding: utf-8 -*-

"""
Detection of USB mass storage units currently pluged in, detection of
newly plugged usb mass storage units.
"""

from subprocess import *
import re

# fsTypes is the list of filesystem type identificators which can be retreived
# from the command "fdisk" in interactive mode.
# 1  FAT12           39  Plan 9          82  Linux swap / So c1  DRDOS/sec (FAT-
# 2  XENIX root      3c  PartitionMagic  83  Linux           c4  DRDOS/sec (FAT-
# 3  XENIX usr       40  Venix 80286     84  OS/2 hidden C:  c6  DRDOS/sec (FAT-
# 4  FAT16 <32M      41  PPC PReP Boot   85  Linux extended  c7  Syrinx
# 5  Extended        42  SFS             86  NTFS volume set da  Non-FS data
# 6  FAT16           4d  QNX4.x          87  NTFS volume set db  CP/M / CTOS / .
# 7  HPFS/NTFS       4e  QNX4.x 2nd part 88  Linux plaintext de  Dell Utility
# 8  AIX             4f  QNX4.x 3rd part 8e  Linux LVM       df  BootIt
# 9  AIX bootable    50  OnTrack DM      93  Amoeba          e1  DOS access
# a  OS/2 Boot Manag 51  OnTrack DM6 Aux 94  Amoeba BBT      e3  DOS R/O
# b  W95 FAT32       52  CP/M            9f  BSD/OS          e4  SpeedStor
# c  W95 FAT32 (LBA) 53  OnTrack DM6 Aux a0  IBM Thinkpad hi eb  BeOS fs
# e  W95 FAT16 (LBA) 54  OnTrackDM6      a5  FreeBSD         ee  GPT
# f  W95 Ext'd (LBA) 55  EZ-Drive        a6  OpenBSD         ef  EFI (FAT-12/16/
#10  OPUS            56  Golden Bow      a7  NeXTSTEP        f0  Linux/PA-RISC b
#11  Hidden FAT12    5c  Priam Edisk     a8  Darwin UFS      f1  SpeedStor
#12  Compaq diagnost 61  SpeedStor       a9  NetBSD          f4  SpeedStor
#14  Hidden FAT16 <3 63  GNU HURD or Sys ab  Darwin boot     f2  DOS secondary
#16  Hidden FAT16    64  Novell Netware  af  HFS / HFS+      fb  VMware VMFS
#17  Hidden HPFS/NTF 65  Novell Netware  b7  BSDI fs         fc  VMware VMKCORE
#18  AST SmartSleep  70  DiskSecure Mult b8  BSDI swap       fd  Linux raid auto
#1b  Hidden W95 FAT3 75  PC/IX           bb  Boot Wizard hid fe  LANstep
#1c  Hidden W95 FAT3 80  Old Minix       be  Solaris boot    ff  BBT
#1e  Hidden W95 FAT1

# Here are the same data processed to make a dictionary

fsTypes={
    '1': 'FAT12',
    '2': 'XENIX root',
    '3': 'XENIX usr',
    '4': 'FAT16 <32M',
    '5': 'Extended',
    '6': 'FAT16',
    '7': 'HPFS/NTFS',
    '8': 'AIX',
    '9': 'AIX bootable',
    'a': 'OS/2 Boot Manag',
    'b': 'W95 FAT32',
    'c': 'W95 FAT32 (LBA)',
    'e': 'W95 FAT16 (LBA)',
    'f': 'W95 Ext\'d (LBA)',
    '10': 'OPUS',
    '11': 'Hidden FAT12',
    '12': 'Compaq diagnost',
    '14': 'Hidden FAT16 <3',
    '16': 'Hidden FAT16',
    '17': 'Hidden HPFS/NTF',
    '18': 'AST SmartSleep',
    '1b': 'Hidden W95 FAT3',
    '1c': 'Hidden W95 FAT3',
    '1e': 'Hidden W95 FAT',
    '39': 'Plan 9',
    '3c': 'PartitionMagic',
    '40': 'Venix 80286',
    '41': 'PPC PReP Boot',
    '42': 'SFS',
    '4d': 'QNX4.x',
    '4e': 'QNX4.x 2nd part',
    '4f': 'QNX4.x 3rd part',
    '50': 'OnTrack DM',
    '51': 'OnTrack DM6 Aux',
    '52': 'CP/M',
    '53': 'OnTrack DM6 Aux',
    '54': 'OnTrackDM6',
    '55': 'EZ-Drive',
    '56': 'Golden Bow',
    '5c': 'Priam Edisk',
    '61': 'SpeedStor',
    '63': 'GNU HURD or Sys',
    '64': 'Novell Netware',
    '65': 'Novell Netware',
    '70': 'DiskSecure Mult',
    '75': 'PC/IX',
    '80': 'Old Minix',
    '82': 'Linux swap / So',
    '83': 'Linux',
    '84': 'OS/2 hidden C:',
    '85': 'Linux extended',
    '86': 'NTFS volume set',
    '87': 'NTFS volume set',
    '88': 'Linux plaintext',
    '8e': 'Linux LVM',
    '93': 'Amoeba',
    '94': 'Amoeba BBT',
    '9f': 'BSD/OS',
    'a0': 'IBM Thinkpad hi',
    'a5': 'FreeBSD',
    'a6': 'OpenBSD',
    'a7': 'NeXTSTEP',
    'a8': 'Darwin UFS',
    'a9': 'NetBSD',
    'ab': 'Darwin boot',
    'af': 'HFS / HFS+',
    'b7': 'BSDI fs',
    'b8': 'BSDI swap',
    'bb': 'Boot Wizard hid',
    'be': 'Solaris boot',
    'c1': 'DRDOS/sec (FAT',
    'c4': 'DRDOS/sec (FAT',
    'c6': 'DRDOS/sec (FAT',
    'c7': 'Syrinx',
    'da': 'Non-FS data',
    'db': 'CP/M / CTOS /',
    'de': 'Dell Utility',
    'df': 'BootIt',
    'e1': 'DOS access',
    'e3': 'DOS R/O',
    'e4': 'SpeedStor',
    'eb': 'BeOS fs',
    'ee': 'GPT',
    'ef': 'EFI (FAT-12/16',
    'f0': 'Linux/PA-RISC',
    'f1': 'SpeedStor',
    'f2': 'DOS secondary',
    'f4': 'SpeedStor',
    'fb': 'VMware VMFS',
    'fc': 'VMware VMKCORE',
    'fd': 'Linux raid aut',
    'fe': 'LANstep',
    'ff': 'BBT'
    }

class partitionDescriptor(object):
    """
    object to represent a disk partiton
    """
    def __init__(self, groups):
        """
        The constructor
        @param groups the tuple (dev,start,stop,blocks,code,comment)
        dev is the device (for example, /dev/sdc2)
        start the start cylinder
        stop the stop cylinder
        blocks the number of blocks
        code the hexadecimal code of the partition type
        comment the name of the partition type
        """
        (dev,start,stop,blocks,code,comment)=groups
        self.dev=dev.replace("/dev/","")
        self.start=start
        self.stop=stop
        self.blocks=blocks
        self.code=code
        self.comment=comment
        cmd="ls -l /dev/disk/by-uuid/ | grep %s| awk '{print $8}'" %self.dev
        uuid = Popen(["/bin/sh", "-c", cmd], stdout=PIPE).communicate()[0]
        uuid=uuid.split("\n")[0]
        self.uuid=uuid

    def equals(self, other):
        """
        @return a boolean, True if the other partition descriptor can be
        considered as the same
        """
        if type(other) != type(self): return False
        return self.dev==other.dev and self.start==other.start and self.stop==other.stop and self.blocks==other.blocks and self.code==other.code and self.uuid==other.uuid

    def __str__(self):
        """
        Stringifying function
        """
        return "%s (%s):%s" %(self.dev,self.size(),self.comment)

    def fsType(self):
        """
        @return the filesystem type, depending on self.code
        """
        return fsTypes[self.code]

    def size(self):
        """
        @return a human-readable string to know the size of the partition
        """
        size=int(self.blocks)
        suf="K"
        if size > 1024:
            size=int(100*size/1024.0)/100.0
            suf="M"
        if size > 1024:
            size=int(100*size/1024.0)/100.0
            suf="G"
        if size > 1024:
            size=int(100*size/1024.0)/100.0
            suf="T"
        return "%s%s" %(size,suf)

    def __repr__(self):
        return self.__str__()

    def whichNum(self):
        """
        @return the number of the chose device, if any. For example if the device is sdd5, it returns 5.
        """
        pattern=re.compile(".*([0-9]+)")
        return int(pattern.match(self.dev).group(1))
    
class storageUnit(object):
    """
    object to represent a USB storage unit
    """
    def __init__(self, dev="", interface=""):
        """
        The constructor
        @param dev the device, maybe in the old-fashioned Unix style
               like "sdb", "sdc", etc.
        @param interface a string like those in /sys/bus/usb/devices/
        """
        self.dev=dev
        self.partitions=self.findPartitions()
        self.interface=interface

    def vendor(self):
        """
        @return the vendor of the storage unit
        """
        cmd="cat /sys/bus/usb/devices/%s/host*/target*/*/vendor" %self.interface
        v=Popen(cmd, shell=True, stdout=PIPE).communicate()[0]
        return v.strip()

    def model(self):
        """
        @return the model of the storage unit
        """
        cmd="cat /sys/bus/usb/devices/%s/host*/target*/*/model" %self.interface
        m=Popen(cmd, shell=True, stdout=PIPE).communicate()[0]
        return m.strip()

    def equals(self, other):
        """
        @return a boolean, True if the other storage unit can be considered
        as the same
        """
        if type(self) != type(other) :      return False
        if self.dev!=other.dev:             return False
        if self.interface!=other.interface: return False
        l=len(self.partitions)
        if l!= len(other.partitions):       return False
        for i in range(l):
            if not self.partitions[i].equals(other.partitions[i]):
                return False
        return True

    def findPartitions(self):
        """
        finds the partitions on the current unit
        @result a list of partition descriptors
        """
        fdiskPattern=re.compile('(/\w+/\w+)\W+(\w+)\W+(\w+)\W+(\w+)\W+(\w+)\W+(.*)')
        result=[]
        cmd="LC_ALL=C sudo fdisk -l /dev/%s | grep '^/dev'" %self.dev
        part = Popen(["/bin/sh", "-c", cmd], stdout=PIPE).communicate()[0]
        part=part.split("\n")[:-1]
        for p in part:
            m=fdiskPattern.match(p)
            if m:
                partition=partitionDescriptor(m.groups())
                result.append(partition)
        return result

    def __str__(self):
        """
        Stringify function
        """
        return "Storage unit (dev=%s, vendor=%s, model=%s, partitions=%s, interface=%s)" %(self.dev, self.vendor(), self.model(), self.partitions, self.interface)

    def __repr__(self):
        """
        Representation function
        """
        return self.__str__()

    def collectPlugged():
        """
        a static method to collect the USB storage units currently plugged
        @result a list of storageUnit objects
        """
        result=[]
        #first find usb-storage interfaces in /sys/bus/usb/devices
        cmd="for f in $(ls /sys/bus/usb/devices/| grep :); do if grep -q usb-storage /sys/bus/usb/devices/$f/uevent; then echo $f; fi; done"
        interfaces = Popen(["/bin/sh", "-c", cmd], stdout=PIPE).communicate()[0]
        interfaces=interfaces.split("\n")[:-1]
        for i in interfaces:
            try:
                cmd="ls -1 /sys/bus/usb/devices/%s/host*/target*/*/block" %i
                block=Popen(["/bin/sh", "-c", cmd], stdout=PIPE).communicate()[0]
                block=block.split("\n")[0]
                result.append(storageUnit(block,i))
            except:
                pass
        return result
    collectPlugged=staticmethod(collectPlugged)

def demo():
    print (storageUnit.collectPlugged())

if __name__=="__main__":
    demo()
