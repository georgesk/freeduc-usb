# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file diskimagetree.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import pickle, os.path

from .import diskimg, utils

class diskImageTree(QTreeWidget):
    """
    a tree widget to display dis images and their contents
    For such a widget, the drag is enabled by default
    """

    def __init__(self, parent):
        """
        The constructor enabled dragging at init time
        """
        QTreeWidget.__init__(self, parent)
        self.setDragDropMode(self.DragOnly)
        self.setDragEnabled(True)
        self.isDirty=False

    def rmEntry(self,img):
        """
        Removes every entry with the same filename than img
        @param img a given diskImg object
        """
        for i in range(self.topLevelItemCount()):
            item=self.topLevelItem(i)
            if item.data.filename==img.filename:
                item=self.takeTopLevelItem(i)
                del item
                self.isDirty=True
                return

    def addFile(self, filename):
        # print "trying to add the file entry:", filename
        img=diskimg.diskImg(filename)
        if img.ok:
            self.addEntry(img)

    def addEntry(self, img):
        """
        add entries for a disk image file.
        @param img : the disk image
        """
        for item in self.findItems("*", Qt.MatchWildcard):
            if item.imgData.filename==img.filename:
                #print "not adding an already existing entry"
                return
        self.isDirty=True
        item=QTreeWidgetItem(self)
        item.setText(0,"%s" %img.filename)
        item.imgData=img
        item.partition=None
        self.addTopLevelItem(item)
        i=0
        for n,p in img.partitions.items():
            item1=QTreeWidgetItem(item)
            item1.setText(0,"%d   %s" %(p.num,p.bootableStar()))
            size=512*p.sectors
            multiplicator=""
            if size > 1024*1024:
                size=int(10*size/1024/1024)/10.0
                mutiplicator="M"
                if size > 1024:
                    size=int(10*size/1024)/10.0
                    multiplicator="G"
            item1.setText(1,"%d%s" %(size,multiplicator))
            item1.setText(2,"%s" %p.start)
            item1.setText(3,"%s" %p.end)
            item1.setText(4,"%s" %p.system)
            # the following line triggers crashes !!!
            # item1.data=img
            # so the field has been renamed imgData instead of data
            item1.imgData=img
            item1.partition=p
            depth=150
            for j in range(5):
                if i%2==0:
                    r,v,b=(255,255,depth)
                else:
                    r,v,b=(depth,255,255)
                r,v,b=utils._fadeColors(r,v,b,j,0,8)
                item1.setBackground ( j, QBrush(QColor(r,v,b)) )
                self.addTopLevelItem(item1)
            i+=1
        item.setExpanded(True)
        item.setBackground ( 0, QBrush(QColor("wheat")) )
        self.setFirstItemColumnSpanned(item, True)
        return

    def mousePressEvent(self,event):
        if event.button() == Qt.LeftButton:
            self.dragStartPosition = event.pos()
            item=self.itemAt(event.pos())
            if item:
                self.data=(item.partition,item.imgData)
        QTreeWidget.mousePressEvent(self,event)

    def  mouseMoveEvent(self,event):
        if (event.pos() - self.dragStartPosition).manhattanLength() < QApplication.startDragDistance():
            return
        drag = QDrag(self);
        mimeData = QMimeData()
        # at that point, we should encode the data from a storage Data
        mimeData.setData("application/x-diskImageData",pickle.dumps(self.data))
        drag.setMimeData(mimeData)
        drag.setPixmap(self.devicePixmap())
        dropAction = drag.exec_()

    def devicePixmap(self, color=QColor(255,200,200)):
        """
        @param color the background color
        @return a pixmap containing a text describing the data of the line
        """
        partition, img=self.data
        max=20 # don't borrow more than 20 chars from the end of the filename
        if len (img.filename)>max:
            text="... "
        else:
            text=""
        text+=img.filename[-max:]
        fm=QFontMetrics (QFont(), self)
        size=fm.size(Qt.TextSingleLine,text+"   ")
        height=fm.height()
        pixmap = QPixmap(size)
        pixmap.fill(color)
        painter=QPainter(pixmap)
        painter.drawText(QPoint(0,height),text)
        painter.end()
        return pixmap

    def save(self, cfgdir):
        """
        Save every relevant data in the configuration directory
        @param cfgdir the configuration directory
        """
        imgFile=open(os.path.join(cfgdir,'imgfiles'),"w")
        for i in range(self.topLevelItemCount()):
            item=self.topLevelItem(i)
            imgFile.write("%s\n" %item.imgData.filename)
        imgFile.close()

