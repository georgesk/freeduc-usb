# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file isolist.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import pickle

import os.path

class isoList(QListWidget):
    """
    a class to have draggable ISO file designations.
    """
    def __init__(self, parent):
        """
        The constructor enabled dragging at init time
        """
        QListWidget.__init__(self, parent)
        self.setDragDropMode(self.DragOnly)
        self.setDragEnabled(True)
        self.isDirty=False

    def addItem(self, label):
        """
        adds a label as a new item when it does not already exist
        @param label the item to add
        """
        for i in range (self.count()):
            item=self.item(i)
            if item.text()==label:
                return
        QListWidget.addItem(self,label)
        self.isDirty=True

    def mousePressEvent(self,event):
        if event.button() == Qt.LeftButton:
            self.dragStartPosition = event.pos()
            item=self.itemAt(event.pos())
            if item:
                self.data=item.text()
        QListWidget.mousePressEvent(self,event)

    def  mouseMoveEvent(self,event):
        if (event.pos() - self.dragStartPosition).manhattanLength() < QApplication.startDragDistance():
            return
        drag = QDrag(self);
        mimeData = QMimeData()
        # at that point, we should encode the data from a storage Data
        mimeData.setData("application/x-isoImageData",pickle.dumps(self.data))
        drag.setMimeData(mimeData)
        drag.setPixmap(self.devicePixmap())
        dropAction = drag.exec_()

    def save(self, cfgdir):
        """
        Save every relevant data in the configuration directory
        @param cfgdir the configuration directory
        """
        isoFile=open(os.path.join(cfgdir,'isofiles'),"w")
        for i in range(self.count()):
            item=self.item(i)
            isoFile.write("%s\n" %item.text())
        isoFile.close()

    def devicePixmap(self, color=QColor(255,200,200)):
        """
        @param color the background color
        @return a pixmap containing a text describing the data of the line
        """
        path=self.data
        max=40 # don't borrow more than 40 chars from the end of the filename
        if len (path)>max:
            text="... "
        else:
            text=""
        text+=path[-max:]
        fm=QFontMetrics (QFont(), self)
        size=fm.size(Qt.TextSingleLine,text+"   ")
        height=fm.height()
        pixmap = QPixmap(size)
        pixmap.fill(color)
        painter=QPainter(pixmap)
        painter.drawText(QPoint(0,height),text)
        painter.end()
        return pixmap
