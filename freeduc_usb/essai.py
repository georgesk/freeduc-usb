# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created: Thu Jul 15 13:17:28 2010
#      by: PyQt4 UI code generator 4.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import sys


from .devicetree import deviceTree
from .dropbutton import dropButton
from .diskimagetree import diskImageTree
from .dropfilebutton import dropFileButton

app = QApplication(sys.argv)
app.mainWindow=QMainWindow(None)
ts = deviceTree(None)
ts.update()
#app.mainWindow.show()
ts.show()
sys.exit(app.exec_())



