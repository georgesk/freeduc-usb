# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file mountpoint.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from . import sudo
import os.path, tempfile, subprocess

class MountPoint:
    """
    implements a mountpoint to be associated with a disk partition
    the mountpoint may be created temporarily when necessary, if so it will
    be appended to a list of mountpoints to clear at application level
    """

    def __init__(self,partition):
        """
        @param partition a variable type data:
        either the disk partition to mount (as a string beginning with "/dev"),
        or a tuple (partition descriptor, image file descriptor),
        or else a path to an ISO image.
        """
        self.partition=partition
        part=str(partition)
        self.mountpoint=None
        self.temporary=False
        #self.isDevice is True when 'partition' is a string
        self.isDevice=(type(partition)==type("") and os.path.exists("/dev/"+partition))
        self.isIso=(part[-4:]==".iso" and
                    os.path.exists(part))
        #checks whether a mountpoint already exists
        mounted=False
        if self.isDevice:
            mp = self.existsMountPoint(self.partition)
            if mp != None:
                self.mountpoint=mp
                self.temporary=False
                mounted=True
        if not mounted:
            if sudo.hasPermission():
                if self.isDevice:
                    mountpoint=tempfile.mkdtemp("","TMP_%s_" %self.partition)
                    sudo.call("mount /dev/%s %s" %(self.partition,mountpoint), shell=True, stderr=None)
                elif self.isIso:
                    path=str(self.partition)
                    # define the name from the filename minus the .iso postfix
                    name=os.path.basename(path)[:-4]
                    mountpoint=tempfile.mkdtemp("","TMP_%s_" %name)
                    sudo.call("mount -o loop,ro -t iso9660 %s %s" %(path,mountpoint), shell=True, stderr=None)
                else:
                    img=self.partition[1]
                    num=self.partition[0].num
                    system=self.partition[0].system
                    f=os.path.basename(img.filename)
                    mountpoint=tempfile.mkdtemp("","TMP_%s_%s_%s" %(f,num,system))
                    img.mount(num, mountpoint)
                self.mountpoint=mountpoint
                self.temporary=True

    def existsMountPoint(device):
        """
        a static function to check if a mount point already exists
        @param device the id of a device without "/dev/"
        @return the path to the mountpoint when it exists, or None
        """
        cmd="grep ^/dev/%s /etc/mtab" %device
        pipe = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        reply=pipe.communicate()[0].split(" ")
        if len(reply)>2:
            mountpoint = reply[1]
            # fix a bug with \040
            mountpoint=mountpoint.replace("\\040"," ")
            return mountpoint
        else:
            return None

    existsMountPoint=staticmethod(existsMountPoint)

    def replyToApp(self):
        """
        The reply to send to an application upon request, to signal
        the mountpoint with all its attributes, including "temporary".
        """
        return {"mountpoint":self}

class CloopMountPoint:
    """
    implements a mountpoint to be associated with a cloop image
    the mountpoint is created temporarily.
    """

    __currentCloop=-1

    def __init__(self, path):
        """
        The constructor
        @param path the path to the cloop image
        """
        self.path=path
        if sudo.hasPermission():
            CloopMountPoint.__currentCloop+=1
            if CloopMountPoint.__currentCloop > 7:
                raise IndexError( "there may not bee more than 8 cloop mounts")
            self.device="cloop%d" %CloopMountPoint.__currentCloop
            ############################"
            # there is a bug with cloop and losetup with kernel 2.6.31
            ############################"
            # sudo.call("losetup /dev/%s %s" %(self.device,self.path), shell=True, stderr=None)
            cmd="modprobe cloop file='%s'" %(self.path)
            print ("About to make a cloop for file %s" %self.path)
            print ("with the command « sudo %s »" %cmd)
            subprocess.call(cmd, shell=True, stderr=None)
            mountpoint=tempfile.mkdtemp("","TMP_%s_" %self.device)
            self.mountpoint=mountpoint
            sudo.call("mount -r /dev/%s %s" %(self.device,mountpoint), shell=True, stderr=None)
        else:
            self.device=None


    def current():
        """
        a static method which returns the current cloop device created.
        """
        return CloopMountPoint.__currentCloop

    current=staticmethod(current)

    def replyToApp(self):
        """
        The reply to send to an application upon request, to signal
        the mountpoint with all its attributes.
        """
        return {"cloopmountpoint":self}

