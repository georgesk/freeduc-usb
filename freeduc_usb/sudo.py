# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file sudo.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import subprocess

def hasPermission():
    """
    Ensures that we have the right of sudoer, asking for a password if necessary
    @result boolean, True if the call is successful.
    """
    retcode = subprocess.call("sudo -n true", shell=True, stderr=None)
    if retcode == 0:
        return True
    while retcode!=0:
        pw,ok=QInputDialog.getText (None, "Input a password", "Password for Sudo:", QLineEdit.Password)
        if ok:
            pipe = subprocess.Popen("sudo -S true",
                                    shell=True,
                                    stdin=subprocess.PIPE)
            pipe.communicate("%s\n" %pw)
            retcode = pipe.wait()
            if retcode==0:
                return True
        else:
            return False

def call(cmd, shell=True, stderr=None):
    """
    invokes subprocess.call with sudo prepended before the command
    @param cmd a command
    @param shell boolean true if we want to encapsulate the command in a shell. Defaults to True
    @param stderr an option to manage the error output. Defaults to None.
    @result the result of subprocess.call, or False if we cannot get the sudo permission.
    """
    if not hasPermission():
                return False
    return subprocess.call("sudo %s" %cmd, shell=shell, stderr=stderr)


