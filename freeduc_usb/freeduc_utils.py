#!/usr/bin/python
# -*- coding: utf-8 -*-

COPYRIGHT="""
Copyright © 2009 Georges Khaznadar <georgesk@ofset.org>
"""
LICENSE="""
This program is part of the package freeduc-toolkit
Freeduc-toolkit was developed to issue easily releases of Freeduc-USB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import curses, string, subprocess

#-- Define a few constants
EXIT = 0
CONTINUE = 1

#-- retuns a curses window with a frame and a title
def newWin(height,width,y,x,title=None):
    s=curses.newwin(height,width,y,x)
    s.box()
    if title:
        s.addstr(0, 3,"| %s |" %title,curses.A_NORMAL)
    return s

#-- a viewer which uses quite all the available screen
def viewer(lines,title):
    offset = 0
    s = newWin(19, 77, 3, 1,title)
    num_lines = len(lines)
    end = 0
    while not end:
        for i in range(1,18):
            if i+offset < num_lines:
                line = string.ljust(lines[i+offset],74)[:74]
            else:
                line = " "*74
                end = 1
            if i<3 and offset>0: s.addstr(i, 2, line, curses.A_BOLD)
            else: s.addstr(i, 2, line, curses.A_NORMAL)
        s.refresh()
        c = s.getch()
        offset = offset+15
    s.erase()
    return CONTINUE

#-- displays a few tips
def tips_func():
    lines="""
To activate a menu:
     type the underlined hotkey.
To get more information from a multi-page help:
     press any key until the help is finished.
To exit:
     type Q.
""".split("\n")
    return viewer(lines,"Help: tips")

def compress_func(cfg_dict):
    cmd='./comprimeFreeduc %s %s %s' %(cfg_dict['uncompressed_dir'][0], cfg_dict['compressed_dir'][0], '/tmp/bigtempdir')
    liste=['xterm','-e',cmd]
    subprocess.call(liste)
    return CONTINUE

