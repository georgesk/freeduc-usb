# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file progressProcess.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import subprocess, threading, sys, os.path, getopt
from .Ui_progress import Ui_MainWindow

class pauseThread(threading.Thread):
    """
    This kind of Thread is supposed to launch a shell command and to
    keep its PID if the call was successful.
    """
    def __init__(self, shellCommand):
        """
        The constructor needs a shell command
        @param shellCommand the shell command
        """
        threading.Thread.__init__(self)
        self.command=shellCommand

    def run(self,):
        self.p=subprocess.Popen(self.command,shell=True,stdout=subprocess.PIPE)
        self.pid=self.p.pid
        print (self.p.poll())


class mainWindow(QMainWindow):
    """
    just a proxy, adding eventually a few local methods
    Ui_MainWindow is designed with the utility "designer" from qt4-tools.
    Do not modify the file Ui_MainWindow.py by hand. Rather edit main.ui
    with "designer" and then "make all".
    """

    def __init__(self, parent=None, argv=[]):
        """
        The constructor
        @param parent a parent window, None by default
        @param argv command-line arguments
        """
        ######QT
        QMainWindow.__init__(self)
        QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.stopButton.setEnabled(False)
        self.ui.pauseButton.setEnabled(False)
        self.pauseIcon0=self.ui.pauseButton.icon()
        self.pauseIcon1=QIcon(os.path.join(self._dir("img"),"qemu-pause1.svg"))
        self.state="stop"

        ####intialize directories
        self._dir()

        #internal connections
        self.ui_connections()



        # consider command line options
        self.considerOptions(argv)

    def ui_connections(self):
        """
        Connect signals with methods
        """
        QObject.connect(self.ui.startButton,SIGNAL("clicked()"), self.startQemu)
        QObject.connect(self.ui.pauseButton,SIGNAL("clicked()"), self.pauseQemu)
        QObject.connect(self.ui.stopButton, SIGNAL("clicked()"), self.stopQemu)

    def startQemu(self):
        """
        Starts Quemu if possible.
        When possible, modifies the status of the toolButtons
        """
        cmd="echo 'this is an exemple'; ls -l"
        self.th=pauseThread(cmd)
        self.th.start()
        ok = self.th.isAlive()
        if ok:
            self.setRunning(True)

    def setRunning(self, state):
        """
        set the state as running or stoppef belonging on the state argument
        @param state boolean True to set the running state
        """
        if state:
            self.state="run"
        else:
            self.state="stop"
        self.ui.startButton.setEnabled(not state)
        self.ui.pauseButton.setEnabled(state)
        self.ui.stopButton.setEnabled(state)


    def pauseQemu(self):
        """
        toggles the state of the child process between running and sleeping
        """
        if self.state=="sleep":
            cmd="kill -CONT %d" %self.th.pid
            subprocess.call(cmd, shell=True)
            self.state="run"
            self.ui.pauseButton.setIcon(self.pauseIcon0)
        else:
            cmd="kill -TSTP %d" %self.th.pid
            subprocess.call(cmd, shell=True)
            self.state="sleep"
            self.ui.pauseButton.setIcon(self.pauseIcon1)

    def stopQemu(self):
        if self.state in ["run","sleep"]:
            cmd="kill -KILL %d" %self.th.pid
            subprocess.call(cmd, shell=True)
            self.setRunning(False)



    def considerOptions(self, argv):
        """
        take in account command line options
        @param argv command line options, not previously parsed
        """
        availableShort='d:hm:'
        availableLong=['diskimage=','help','mem=']
        try:
            optlist, args = getopt.getopt(argv, availableShort, availableLong)
        except getopt.GetoptError as err:
            print (str(err)) # will print smth like "option -a not recognized"
            self.usage()
            sys.exit(2)
        for o in optlist:
            k,v=o
            if k=='-h' or k=='--help':
                self.usage()
                sys.exit(2)
        return

    def _dir(which=None):
        """
        @param which a keyword to get some noticeable directory.
        Recognized keywords are: lang,
        @return some noticeable directory
        """
        if which=="lang":
            rep_install= os.path.dirname(os.path.abspath(__file__))
            for dir in (os.path.join(rep_install, 'lang'),
                        '/usr/share/freeduc-usb-manager/lang'):
                if os.path.exists(dir):
                    return dir
            return ""
        elif which=="img":
            for d in ["images","/usr/share/python-freeduc-usb/images"]:
                if os.path.exists(d):
                    return d
        return None

    _dir=staticmethod(_dir)

def run():
    ### take in account options
    argv=sys.argv[1:]

    app = QApplication(sys.argv)
    ###translation##
    locale = "%s" %QLocale.system().name()
    #locale = "%s" %QString("en_EN")

    qtTranslator = QTranslator()
    if qtTranslator.load("qt_" + locale):
        app.installTranslator(qtTranslator)
    appTranslator = QTranslator()
    langdir=os.path.join(mainWindow._dir("lang"),
                         "freeduc-usb-manager_"+locale)
    if appTranslator.load(langdir):
        b = app.installTranslator(appTranslator)

    w = mainWindow(None, argv)
    w.show()
    sys.exit(app.exec_())

if __name__=="__main__":
    run()
