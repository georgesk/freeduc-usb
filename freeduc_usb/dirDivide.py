#! /usr/bin/python
# -*- coding: utf-8 -*-

from subprocess import *
import sys, getopt, copy, re
from optparse import OptionParser

"""
\brief Purpose : this package is useful to divide a directory's content into
lists of "pathspecs"

Pathspecs are in a format usable by the command genisomage which should give
approximately equal sized iso images from one source directory. This is useful for directories whose contents are bigger than approximately 2 gigabytes, since such sizes usually yeld iso images trespassing the threshold of allowed size for the format iso9660
"""

class pathSpec:
     """\brief a class to encapsulated pathspecs
     """
     def __init__(self,size=0,path=[]):
         """The constructor

         \param size a size in kilobytes
         \param path a path to a file or a directory
         """
         self._size=size
         if type(path)==type(""):
             self._path=[path]
         else:
             self._path=copy.copy(path)
     def __str__(self):
         """The stringifying function"""
         return "pathSpec(%d,%s)" %(self._size, self._path)
     def __repr__(self):
         """The reopresentation string"""
         return self.__str__()

     def __add__(self,other):
         """The addition operator"""
         result=pathSpec(self._size,self._path)
         result._size+=other._size
         result._path+=copy.copy(other._path)
         return result
     def size(self):
         return self._size
     def path(self):
         return self._path
     def spacedPath(self):
         return " ".join(self._path)
     def graftedPath(self,root):
         """A routine to output pathSpecs in a mode which can be accepted
         by genisoimage with the option -graft-points
         """
         if self._path==[root]:
              return ".=%s" %root
         result=""
         pattern=root+"/(.*)"
         ex=re.compile(root+"/(.*)")
         for p in self._path:
             m=ex.match(p)
             try:
                  unrooted=m.group(1)
                  result+="%s=%s " %(unrooted,p)
             except:
                  print ("problem with root=%s" %root)
                  print ("probably this dir name contains keychars for regexps")
                  import sys; sys.exit(0)
         return result

def getPathSpecs (dir, maxSize, result=[pathSpec()]):
    """\brief a function to divide a directory into paths of equal weight

    The paths are accesses to files which totalize approximately the given
    size of data

    \param dir the input directory
    \param size the size of data to be found in a pathspec
    \param result a list of pathspech, each totalizing at most size bytes of contents. This one is returned by the function.
    """

    contents=Popen(["du", "-s", dir], stdout=PIPE).communicate()[0]
    contentsize=int(contents.split("\t")[0])
    if contentsize < maxSize:
        return [pathSpec(contentsize,[dir])]
    else:
        contents=Popen(["sh", "-c", "du -s %s/*" %dir], stdout=PIPE).communicate()[0]
        contents=contents.split("\n")[:-1]
        for c in contents:
            sizedir=c.split("\t")
            size=int(sizedir[0])
            dir=sizedir[1]
            ps=pathSpec(size,dir)
            added=False
            for i in range(len(result)):
                if result[i].size()+size <maxSize:
                    result[i]+=ps
                    added=True
                    break
            if not added:
                if ps.size() < maxSize:
                    result.append(ps)
                else:
                    result=getPathSpecs(dir,maxSize,result)
        return result

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-r", "--root",
                      action="store", type="string", dest="root", default="",
                      help="considers ROOT as the root of the iso image", metavar="ROOT")
    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose", default=True,
                      help="don't print verbose messages")

    (opts, args) = parser.parse_args()

    if len(sys.argv)>2:
         maxsize=int(args[1])
    else:
         maxsize=2000000 # maxsize is 2 gigabytes by default
    pathspecs=getPathSpecs(args[0],maxsize)
    if opts.verbose:
        print ("""\
//===========================================================================//
// If you want to call this script from a shell in order to make ISO images: //
//                                                                           //
// dirDivide.py --quiet somedir 2000000 |                                    //
//   (read p; while [ -n "$p" ]; do genisoimage [options] $p; read p; done)  //
//===========================================================================//\
""")
    for ps in pathspecs:
        if opts.verbose: print ("//========", ps.size()/1024, "megabytes", "========//")
        if opts.root != "":
             print (ps.graftedPath(opts.root))
        else:
             print (ps.graftedPath(args[0]))

