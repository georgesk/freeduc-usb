# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file utils.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

def _fadeColors(r,v,b,level,min,max):
    """
    @param r the red component
    @param v the green component
    @param b the blue component
    @param level  a level of fading
    @param min when level==min the colors are not faded
    @param max when j==max the colors are completely faded to white
    @return a triplet of colors components faded to the white
    """
    fading=lambda x:x+(255-x)*(level-min)/(max-min)
    return fading(r), fading(v), fading(b)
