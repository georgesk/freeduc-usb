# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file dropdirbutton.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .dropbutton import dropButton
import pickle

class dropDirButton(dropButton, QAbstractItemModel):
    """
    a tool button able to accept drops fron the treeview
    """
    def __init__(self,
                 parent=None,
                 mFormat="application/x-freeducProjectData"):
        """
        the constructor sets the acceptance for drops,
        connects the click signal to an explanation,
        and defines a default no-do action for them.
        """
        QAbstractItemModel.__init__(self,None)
        dropButton.__init__(self, parent, mFormat)
        self.setAcceptDrops(True)
        self.acceptFormat=mFormat
        self.setDropAction(lambda x: x)
        self.connect(self,SIGNAL("clicked()"),self.explain)

