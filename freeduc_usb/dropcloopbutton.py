# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file dropcloopbutton.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import pickle


class dropCloopButton(QToolButton, QAbstractItemModel):
    """
    a tool button able to accept drops fron the treeview
    """
    def __init__(self,
                 parent=None,
                 mFormat="application/x-cloopData"):
        """
        the constructor sets the acceptance for drops,
        connects the click signal to an explanation,
        and defines a default no-do action for them.
        """
        QAbstractItemModel.__init__(self, None)
        QToolButton.__init__(self, parent)
        self.setAcceptDrops(True)
        self.acceptFormat=mFormat
        self.setDropAction(lambda x: x)
        self.connect(self,SIGNAL("clicked()"),self.explain)

    def explain(self):
        mb=QMessageBox.information(None,
                                   "This is a drop button",
                                   "This is a drop button. Drop items from above to get something done.\n %s" %self.toolTip())

    def dragEnterEvent(self, event):
        """
        says whether the drop event might be accepted
        """
        if event.mimeData().hasFormat(self.acceptFormat):
            event.acceptProposedAction()

    def dropEvent(self, event):
        """
        Accepts the drop
        """
        md=event.mimeData()
        machin=md.data(self.acceptFormat)
        result=self.dropAction(pickle.loads(machin))
        mainWindow=QToolButton.parent(self).parent()
        while 'ui' not in mainWindow.__dict__.keys():
            mainWindow=mainWindow.parent()
        mainWindow.postProcess(result)
        event.acceptProposedAction()

    def setDropAction(self,action):
        """
        defines the action to be done upon drops.
        @param action a function accepting one parameter, which is a dictionary.
        This dictionary has a key "directory" and another optional key "fname"
        """
        self.dropAction=action


