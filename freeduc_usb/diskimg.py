# -*- Coding: utf-8 -*-


from subprocess import *
import os,os.path,re,tempfile

class diskPartition:
    """
    a class to represent disk partitions
    """
    def __init__(self,data):
        """
        the constructor
        @param data a tuple containing:
        - num     the number of a partition inside a device
        - boot    boolean, bootable flag
        - start   start sector
        - end     end sector
        - sectors count of sectors
        - id      hexadecimal id of the partition
        - system  human-readable type of the partition
        """
        (num,boot,start,end,sectors,id,system)=data
        self.num=int(num)
        self.boot=boot!=None
        self.start=int(start)
        self.end=int(end)
        self.sectors=int(sectors)
        self.id=id
        self.system=system


    def __str__(self):
        return "#%s boot=%s start=%s end=%s sectors=%s id=%s system=%s" %(self.num,self.boot,self.start,self.end,self.sectors,self.id,self.system)

    def bootableStar(self):
        """
        @ return a star in a string if the partition is bootable
        """
        if self.boot: return "*"
        else: return " "

    def fromSfdiskLine(d, filename):
        """
        a constructor
        @param d a line, hopefully in the syntax of sfdisk
        @result a diskPartition instance or None if not possible
        """
        pattern=re.compile("%s([1-9])( +\*)? +([0-9]+) +([0-9]+) +([0-9]+) +([0-9a-f]+) +(.*)" %filename)
        m=pattern.match(d)
        if m:
            return diskPartition(m.groups())
        else:
            return None

    fromSfdiskLine=staticmethod(fromSfdiskLine)


class diskImg:
    """
    a class to manage disk images in a file
    """

    def __init__(self, filename):
        """
        The constructor
        @param filename the file named so must contain the disk image
        another file named filename+".sfdisk" must exist. This file is the
        record output by sfdisk about the original disk whose image
        was copied.
        """
        self.filename=os.path.abspath(filename)
        self.ok=os.path.exists(filename) and self.cylinders()!=0
        if self.ok:
            self.partitions=self.structure()
        else:
            self.partitions=None
        self.mounted={}

    def copy(self):
        """
        @return a copy of self as another instance
        """
        return diskImg(self.filename)

    def mount(self,n,mountpoint):
        """
        mounts a partition
        @param n number of the partition
        @param mountpoint the directory where to mount the partition
        """
        n=int(n)
        if n in self.partitions.keys() and n not in self.mounted.keys() and self.partitions[n].id !='5':
            cmd="sudo mount -o loop,offset=%d  %s '%s'" %(self.partitions[n].start*512, self.filename, mountpoint)
            if call(cmd, shell=True)==0:
                self.mounted[n]=mountpoint

    def umount(self,n):
        """
        unmounts a partition
        @param the number of the partition
        """
        if n in self.mounted.keys():
            cmd="sudo umount '%s'" %self.mounted[n]
            if call(cmd,shell=True)==0:
                del self.mounted[n]

    def __str__(self):
        return "Disk image in %s, ok=%s" %(self.filename, self.ok)

    def fileControl(self):
        """
        @return the filename of the control file
        """
        return self.filename+".sfdisk"

    def cylinders(self):
        """
        @return the number of cylinders of the disk image
        """
        cmd="sed -n 's/.* \([0-9]\+\) cylind.*/\\1/ p' %s" %self.fileControl()
        return int(Popen(cmd, shell=True, stdout=PIPE).communicate()[0][:-1])

    def verify(self):
        f=self.filename
        if os.path.dirname(f)==os.getcwd():
            f=os.path.basename(f)
        cmd="sudo sfdisk -C %d -l -uS %s" %(self.cylinders(), f)
        call(cmd, shell=True)

    def structure(self):
        """
        Gets the structure of the disk image
        """
        f=self.filename
        d=os.path.dirname(f)
        f=os.path.basename(f)
        cmd="cd %s; sudo sfdisk -C %d -l -uS %s" %(d,self.cylinders(),f)
        data=Popen(cmd, shell=True,stdout=PIPE,stderr=STDOUT).communicate()[0]
        data=data.split("\n")[:-1]
        result={}
        for d in data:
            part=diskPartition.fromSfdiskLine(d,f)
            if part:
                result[part.num]=part
        return result


if __name__=="__main__":
    file="../examples/example_usb_disk.img"
    img=diskImg(file)
    if img.ok:
        print ("Found a disk image in %s" %file)
        for (k,v) in img.structure().items():
            print ("%s %s" %(k,v))
        # shows the contents of partitions 1 and 5
        d=tempfile.mkdtemp()
        d1=os.path.join(d,"1")
        d5=os.path.join(d,"5")
        os.mkdir(d1)
        os.mkdir(d5)
        img.mount(1,d1)
        img.mount(5,d5)
        call("ls %s %s" %(d1,d5), shell=True)
        img.umount(1)
        img.umount(5)
        call ("rm -rf %s" %d, shell=True)

