# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file actions.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import os.path, tempfile
from .mountpoint import MountPoint, CloopMountPoint

from . import sudo, diskimg

import subprocess, threading



def displayPartitionContents(storageDeviceData):
    """
    checks whether /dev/parta partition is already mounted; on the contrary,
    creates a tmpdir, and mounts the partition on behalf of root;
    launches nautilus to browse the partition.
    @param storageDeviceData a tuple ("dev", partition descriptor)
    """
    if storageDeviceData[0]=="dev":
        mb=QMessageBox.information(None,
                                   "Operation not possible",
                                   "This tool must be fed with partitions, please pick one of the tree leaves.")
    else:
        if storageDeviceData[1].code=="5":  #extended partition
             mb=QMessageBox.information(None,
                                        "Operation not possible",
                                        "The partition is an extended partition,\nplease choose a non-extended partition.")
             return
        part=storageDeviceData[1].dev
        mp=MountPoint(part)
        subprocess.call("nautilus '%s'" %mp.mountpoint, shell=True, stderr=None)
        return mp.replyToApp()

def displayIsoContents(isoFileName):
    """
    creates a tmpdir, and mounts the ISO image on behalf of root;
    launches nautilus to browse the partition.
    @param isoFileName a path to an ISO image
    """
    mp=MountPoint(isoFileName)
    subprocess.call("nautilus '%s'" %mp.mountpoint, shell=True, stderr=None)
    return mp.replyToApp()

def displayCloopContents(cloopData):
    """
    creates a tmpdir, and mounts the cloop image on behalf of root;
    launches nautilus to browse the partition.
    @param cloopData a path to a cloop image
    """
    if "fname" in cloopData.keys(): # this is a single cloop file
        path=os.path.join(cloopData["directory"], cloopData["fname"])
        cloopMp=CloopMountPoint(path)
        subprocess.call("nautilus '%s'" %cloopMp.mountpoint, shell=True, stderr=None)
        return cloopMp.replyToApp()

def displayFilePartitionContents(data):
    """
    Displays the file contents in a partition belonging to a disk image
    """
    mp=MountPoint(data)
    cmd="nautilus '%s'" %mp.mountpoint
    th=threading.Thread(target=subprocess.call, args=(cmd,),
                        kwargs={"shell":True, "stderr":None})
    th.start()
    return mp.replyToApp()

def displayChrootContents(data):
    """
    Displays the file contents in a partition belonging to a disk image
    """
    cmd="nautilus '%s'" %os.path.join("%s" %data, "live")
    th=threading.Thread(target=subprocess.call, args=(cmd,),
                        kwargs={"shell":True, "stderr":None})
    th.start()
    return None

def partitionEditor(storageDeviceData):
    if storageDeviceData[0]!="dev":
        mb=QMessageBox.information(None,
                                   "Operation not possible",
                                   "This tool must be fed with a root of a storage device, do not choose leaves of the above tree.")
    else:
        if sudo.hasPermission():
            storage=storageDeviceData[1]
            th=threading.Thread(target=subprocess.call, args=("sudo gparted /dev/%s" %storage.dev,), kwargs={"shell":True, "stderr":None})
            th.start()


def makeBootable(storageDeviceData):
    """
    make a device bootable
    @param storageDeviceData the description of the device
    """
    if storageDeviceData[0]!="dev":
        mb=QMessageBox.information(None,
                                   "Operation not possible",
                                   "This tool must be fed with a root of a storage device, do not choose leaves of the above tree.")
    else:
        storage=storageDeviceData[1]
        linuxParts=[]
        for p in storage.partitions:
            if p.comment == "Linux":
                linuxParts.append(p)
        if len(linuxParts)>0:
            return bootInstall(storage, linuxParts[0])
        else:
            return None

def bootInstall(dev, place, kbdencoding="french.kbd", lang="fr"):
    """
    installs boot records in the main sector and the first sector of a partition
    for a USB stick, and Extlinux in the chosen partition
    @param dev the device (type = detector.storageUnit)
    @param place one if its partitions (must be of type Linux, with a filesystem based on ext2, ext3)
    @param kbdencoding the filename of a keybord encoding for extlinux
    @param lang the language to pass to KNOPPIX at boot time
    """
    mb=QMessageBox.information(None,
                               "Choose a source directory for extlinux",
                               "You will install Extlinux bootloader into %s's MBR, and the directories into %s.\nThe first stage is to choose a directory which contains template files to use with Extlinux, like keyboard layouts, a splash image, etc." %(dev.dev, place.dev))
    sourcedir=QFileDialog.getExistingDirectory (None, "Choose a source directory for extlinux.")
    mp=MountPoint(place.dev)
    # effacements
    cmd="sudo mkdir -p %s/boot/extlinux; sudo rm -rf %s/boot/extlinux/* 2>/dev/null" %(mp.mountpoint,mp.mountpoint)
    print (cmd)
    subprocess.call(cmd, shell=True, stderr=None)
    # mise en place du répertoire extlinux
    cmd="sudo rsync -a %s/* %s/boot/extlinux 2>/dev/null" %(sourcedir,mp.mountpoint)
    print (cmd)
    subprocess.call(cmd, shell=True, stderr=None)
    tmphandle, tmpname = tempfile.mkstemp()
    # personnalisation de extlinux.conf.sample pour prendre en compte
    # le clavier et la langue
    cmd="sed -e 's/@KBD@/%s/' -e 's/@LANG@/%s/' /usr/share/python-freeduc-usb/extlinux.conf.sample > %s; sudo install -m 644 %s %s/boot/extlinux/extlinux.conf; rm -f %s" %(kbdencoding, lang, tmpname, tmpname, mp.mountpoint, tmpname)
    print (cmd)
    subprocess.call(cmd, shell=True, stderr=None)
    # installation de extlinux dans le boot record de la partition ext2
    cmd="sudo extlinux --install %s/boot/extlinux" %(mp.mountpoint)
    print (cmd)
    subprocess.call(cmd, shell=True, stderr=None)
    # installation du Master Boot Record
    cmd="sudo dd if=/usr/lib/extlinux/mbr.bin of=%s" %dev.dev
    print (cmd)
    subprocess.call(cmd, shell=True, stderr=None)
    return mp.replyToApp()



def partitionQemuLauncher(storageDeviceData):
    if storageDeviceData[0]!="dev":
        mb=QMessageBox.information(None,
                                   "Operation not possible",
                                   "This tool must be fed with a root of a storage device, do not choose leaves of the above tree.")
    else:
        storage=storageDeviceData[1]
        th=threading.Thread(target=subprocess.call, args=("freeduc-qemu  --diskimage=/dev/%s --mem=1G" %storage.dev,), kwargs={"shell":True, "stderr":None})
        th.start()

def fileQemuLauncher(data):
    """
    launches a Qemu session with an image file
    @param img a diskimg.diskImg instance
    """
    partition,img=data
    f=img.filename
    th=threading.Thread(target=subprocess.call, args=("freeduc-qemu  --diskimage=%s --mem=1G" %f,), kwargs={"shell":True, "stderr":None})
    th.start()

def isoQemuLauncher(data):
    """
    launches a Qemu session with an image file
    @param img a diskimg.diskImg instance
    """
    th=threading.Thread(target=subprocess.call, args=("freeduc-qemu  --isoimage=%s --mem=1G" %data,), kwargs={"shell":True, "stderr":None})
    th.start()

def chrootToLive(data):
    """
    chroots into the "live" directory of a project
    WARNING !!! it depends on a file /home/images/.pbuilderrc which is NOT
    currently installed by the package !!!
    """
    th=threading.Thread(target=subprocess.call, args=("/usr/share/python-freeduc-usb/scripts/chrootToImage %s" %data /home/images,), kwargs={"shell":True, "stderr":None})
    th.start()

def pluggedUsbIds():
    """
    @return a list of the identifiers of currently plugged USB memory drives
    """
    cmd="freeduc-barclone | tail -n +6"
    pipe = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    reply=pipe.communicate()[0].split()
    result=[]
    for r in reply:
        if len(r)>0:
            result.append(r)
    return result

def fileBarCloneLauncher(data):
    """
    launches a freeduc-barclone session with an image file
    @param img a diskimg.diskImg instance
    """
    partition,img=data
    f=img.filename
    prompt="""\
Here is a list of currently
plugged USB drives, please
type a suitable pattern to
choose the drives you want to
consider as clone targets

"""
    prompt+=(",\n").join(pluggedUsbIds())
    pattern, ok = QInputDialog.getText(None, 'Enter a search patter', prompt)
    if ok:
        th=threading.Thread(target=subprocess.call, args=("xterm -e freeduc-barclone %s %s" %(pattern,f),), kwargs={"shell":True, "stderr":None})
        th.start()
    else:
        print ("nothing to do")

