# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file clooptree.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import subprocess, os.path, pickle
import sys
from . import utils, sudo

class cloopTree(QTreeWidget):
    """
    a treeWidget to display mass storage devices
    For such a widget, the drag is enabled by default
    """

    def __init__(self, parent):
        """
        The constructor enables dragging at init time
        """
        QTreeWidget.__init__(self, parent)
        self.setDragDropMode(self.DragOnly)
        self.setDragEnabled(True)
        self.dirs=[]

    def findInMounted(self,mountpoints):
        """
        finds every KNOPPIX cloop file by searchin the list of mountpoints
        @param mountpoints a list of MountPoint objects
        """
        # self.clear()
        for i in range(self.topLevelItemCount()):
            item=self.takeTopLevelItem(0)
            del item
        for m in mountpoints:
            found=self.findKnoppix(m.mountpoint)
            if len(found)>0:
                item=QTreeWidgetItem(self)
                item.setText(0,m.mountpoint)
                item.devData={"directory":m.mountpoint}
                i=0
                for f in found:
                    item1=QTreeWidgetItem(item)
                    item1.setText(0,"")
                    fName=os.path.join("KNOPPIX",os.path.basename(f))
                    item1.devData={"directory":m.mountpoint,"fname":fName}
                    item1.setText(1,fName)
                    depth=150
                    for j in range(5):
                        if i%2==0:
                            r,v,b=(255,255,depth)
                        else:
                            r,v,b=(depth,255,255)
                        r,v,b=utils._fadeColors(r,v,b,j,0,8)
                        item1.setBackground ( j, QBrush(QColor(r,v,b)) )
                    self.addTopLevelItem(item1)
                    i+=1
                item.setExpanded(True)
                item.setBackground ( 0, QBrush(QColor("wheat")) )
                self.setFirstItemColumnSpanned(item, True)

    def mousePressEvent(self,event):
        if event.button() == Qt.LeftButton:
            self.dragStartPosition = event.pos()
            item=self.itemAt(event.pos())
            if item:
                self.data=item.devData
        QTreeWidget.mousePressEvent(self,event)

    def  mouseMoveEvent(self,event):
        if (event.pos() - self.dragStartPosition).manhattanLength() < QApplication.startDragDistance():
            return
        drag = QDrag(self);
        mimeData = QMimeData()
        # at that point, we should encode the data from cloop file
        mimeData.setData("application/x-cloopData",pickle.dumps(self.data))
        drag.setMimeData(mimeData)
        drag.setPixmap(self.cloopPixmap())
        dropAction = drag.exec_()

    def cloopPixmap(self, color=QColor(255,200,200)):
        """
        @param color the background color
        @return a pixmap containing a text describing the data of the line
        """
        d=self.data
        k=d.keys()
        text=d["directory"]
        if "fname" in k:
            text+="/"+d["fname"]
        fm=QFontMetrics (QFont(), self)
        size=fm.size(Qt.TextSingleLine,text)
        height=fm.height()
        pixmap = QPixmap(size)
        pixmap.fill(color)
        painter=QPainter(pixmap)
        painter.drawText(QPoint(0,height),text)
        painter.end()
        return pixmap

    def findKnoppix(self, dir):
        """
        find files with the pattern KNOPPIX/KNOPPIX*
        @param dir a directory to search
        @return a list of files fount in this directory
        """
        output = subprocess.Popen("ls %s/KNOPPIX/KNOPPIX* 2>/dev/null | sort" %dir, shell=True, stdout=subprocess.PIPE).communicate()[0]
        files=output.split("\n")[:-1]
        result=[]
        for f in files:
            result.append(f.strip())
        return result

if __name__=="__main__":
    app = QApplication([])
    dt=cloopTree(None)
    for j in range(5):
        print (dt._fadeColors(255,255,200,j,0,16))
