# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    file devicetree.py: this file is part of the package freeduc-usb-manager

    freeduc-usb-manager is a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import pickle,re,subprocess

from . import detector, utils, sudo
from .mountpoint import MountPoint

class deviceTree(QTreeWidget):
    """
    a treeWidget to display mass storage devices
    For such a widget, the drag is enabled by default
    """

    def __init__(self, parent, allowAutomount=False):
        """
        The constructor enabled dragging at init time
        @param parent a QWidget instance
        @param allowAutomount if True, automounted partitions are allowed
         if False, automounted partitions are unmounted as soon they are
         detected. Defaults to False
        """
        QTreeWidget.__init__(self, parent)
        self.setDragDropMode(self.DragOnly)
        self.setDragEnabled(True)
        self.plugged=[]
        self.allowAutomount=allowAutomount

    def samePlugged(self,test):
        """
        @result boolean, True if the list of plugged devices did not change
        """
        if len(test) != len(self.plugged): return False
        for i in range(len(test)):
            a=self.plugged[i]; b=test[i]
            if not a.equals(b): return False
        return True

    def update(self):
        """
        Updates the tree of plugged memory sticks
        """
        try:
            if not sudo.hasPermission():
                raise ValueError
            testPlugged=detector.storageUnit.collectPlugged()
            if self.samePlugged(testPlugged): return
            self.plugged=testPlugged
            for i in range(self.topLevelItemCount()):
                item=self.takeTopLevelItem(0)
                del item
            for pl in self.plugged:
                item=QTreeWidgetItem(self)
                item.setText(0,"%s (%s_%s)" %(pl.dev,pl.vendor(),pl.model()))
                item.devData=("dev",pl)
                self.addTopLevelItem(item)
                i=0
                for part in pl.partitions:
                    item1=QTreeWidgetItem(item)
                    item1.setText(0,"%s" %part.dev)
                    # at that point, we must update the mountpoints list
                    # in the main structure to take in account possibly
                    # already mounted partitions.
                    mp=MountPoint.existsMountPoint(part.dev)
                    if mp != None:
                        automounted=re.compile("^/media/").match(mp)
                        if automounted and self.allowAutomount:
                            self.mountpoints.append(MountPoint(part.dev))
                        else:
                            cmd="umount %s" %mp
                            subprocess.call(cmd, shell=True)
                    item1.setText(1,"%s" %part.size())
                    item1.setText(2,"%s" %part.start)
                    item1.setText(3,"%s" %part.stop)
                    item1.setText(4,"%s" %part.fsType())
                    item1.devData=("part",part)
                    depth=150
                    for j in range(5):
                        if i%2==0:
                            r,v,b=(255,255,depth)
                        else:
                            r,v,b=(depth,255,255)
                        r,v,b=utils._fadeColors(r,v,b,j,0,8)
                        item1.setBackground ( j, QBrush(QColor(r,v,b)) )
                    self.addTopLevelItem(item1)
                    i+=1
                item.setExpanded(True)
                item.setBackground ( 0, QBrush(QColor("wheat")) )
                self.setFirstItemColumnSpanned(item, True)
        except ValueError:
            mb=QMessageBox.information(self, "Password not suitable for sudo",
                                       "You did not provide a password enabling you to get sufficient privileges with sudo.")
            return
        except:
             mb=QMessageBox.information(self, "Some error occurred",
                                       "Which has nothing to do with 'sudo'.")

    def mousePressEvent(self,event):
        if event.button() == Qt.LeftButton:
            self.dragStartPosition = event.pos()
            item=self.itemAt(event.pos())
            if item:
                self.data=item.devData
        QTreeWidget.mousePressEvent(self,event)

    def  mouseMoveEvent(self,event):
        if (event.pos() - self.dragStartPosition).manhattanLength() < QApplication.startDragDistance():
            return
        drag = QDrag(self);
        mimeData = QMimeData()
        # at that point, we should encode the data from a storage Data
        mimeData.setData("application/x-storageDeviceData",pickle.dumps(self.data))
        drag.setMimeData(mimeData)
        drag.setPixmap(self.devicePixmap())
        dropAction = drag.exec_()

    def devicePixmap(self, color=QColor(255,200,200)):
        """
        @param color the background color
        @return a pixmap containing a text describing the data of the line
        """
        d=self.data[1]
        if self.data[0]=="dev":
            text="%s (%s_%s)" %(d.dev,d.vendor(),d.model())
        else:
            text=d.__repr__()
        fm=QFontMetrics (QFont(), self)
        size=fm.size(Qt.TextSingleLine,text)
        height=fm.height()
        pixmap = QPixmap(size)
        pixmap.fill(color)
        painter=QPainter(pixmap)
        painter.drawText(QPoint(0,height),text)
        painter.end()
        return pixmap


if __name__=="__main__":
    app = QApplication([])
    dt=deviceTree(None)
    for j in range(5):
        print (dt._fadeColors(255,255,200,j,0,16))
