# -*- coding: utf-8 -*-
licence={}
licence['en']="""
    freeduc-usb-manager version %s:

    a program to manage USB nomadic live systems

    Copyright (C) 2010 Georges Khaznadar <georgesk@ofset.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys, os

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .main import mainWindow

def run():
    ### take in account options
    argv=sys.argv[1:]

    app = QApplication(sys.argv)
    ###translation##
    locale = "%s" %QLocale.system().name()
    #locale = "%s" %QString("en_EN")

    qtTranslator = QTranslator()
    if qtTranslator.load("qt_" + locale):
        app.installTranslator(qtTranslator)
    appTranslator = QTranslator()
    langdir=os.path.join(mainWindow._dir("lang"),
                         "freeduc-usb_"+locale)
    if appTranslator.load(langdir):
        b = app.installTranslator(appTranslator)

    app.mainWindow = mainWindow(None, argv)
    app.mainWindow.show()
    sys.exit(app.exec_())

if __name__=="__main__":
    run()
