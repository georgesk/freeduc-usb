# -*- coding: utf-8 -*-

from subprocess import *
import re, tempfile, os, sys

class UsbStick:
    """
    A class to manage USB memory sticks
    to make freeduc-USB live media
    """
    def __init__(self,dev):
        """
        The constructor
        will define some useful constants like:
        - sizeK, the size of the device                       (Kbytes)
        - vfatK, the size to be formatted as a vfat partition (Kbytes)
        - ext2K, the size to be formattes as a axt2 partition (Kbytes)
        @param dev the device (for example "/dev/sdd"
        """
        self.dev=dev
        self.sizeK = int(Popen(["sudo", "sfdisk", "-s", dev], stdout=PIPE).communicate()[0])
        self.vfatK = self.recommandedVfat(self.sizeK)
        self.ext2K = self.sizeK-self.vfatK

    def recommandedVfat(self,sizeK):
        """
        @return the recommanded size for the vfat partition
        here is an illustration of the function we define:
gnuplot> f(x)=(x**2-0.6)/(4*x); set term dumb; set xrange [1:4]; set yrange [0:1]; plot f(x)
Terminal type set to 'dumb'
Options are 'feed  79 24'


      1 ++---------+-----------+----------+----------+-----------+---------++
        +          +           +          +          +          f(x) ********
        |                                                           ******  |
        |                                                       *****       |
    0.8 ++                                                  *****          ++
        |                                               *****               |
        |                                          *****                    |
        |                                      *****                        |
    0.6 ++                                 ****                            ++
        |                              ****                                 |
        |                          ****                                     |
        |                      ****                                         |
    0.4 ++                 ****                                            ++
        |              *****                                                |
        |           ***                                                     |
        |       ****                                                        |
    0.2 ++   ****                                                          ++
        | ***                                                               |
        **                                                                  |
        +          +           +          +          +           +          +
      0 ++---------+-----------+----------+----------+-----------+---------++
        1         1.5          2         2.5         3          3.5         4

        """
        sizeG=1.0*sizeK/1024/1024  # conversion: the constants below are in GB
        vfatG=(sizeG**2-0.6)/(4*sizeG)
        # gives 100MB for 1GB, with an asymptote at sizeG/4 further
        return int(vfatG*1024*1024)

    def partedCmd(self):
        """
        @result a command using parted to patition the device
        """
        result=self.dev
        result+=" unit MB"
        result+=" mkpart primary fat32 1 %d" %(self.vfatK/1024)
        result+=" mkpart extended %d %d" %(self.vfatK/1024+1, self.sizeK/1024-1)
        result+=" mkpart logical ext2 %d %d" %(self.vfatK/1024+1, self.sizeK/1024-1)
        result+=" mkfs 1 fat32"
        result+=" mkfs 5 ext2"
        result+=" set 1 boot on"
        return "sudo parted --script "+result

    def mountDir(self):
        """
        @result the mount point of the device to be partitioned
        """
        result=open("/proc/mounts","r").read()
        result=result.split("\n")
        expr=re.compile(".*"+self.dev+".*")
        l=[elem for elem in result if expr.match(elem)]
        if len(l)==0:
            return None
        else:
            return l[0].split(" ")[1]

    def cloneKnoppix(self,fromDir):
        """
        Launches the cloning of the complete structure for the Linux
        partition, except the creation of the persitent data area
        @param fromDir the directory where lives the model partition contents
        @result True in case of success
        """
        mountDir=self.mountDir()
        if mountDir:
            return call("rsync -a --exclude '*data.img' %s/* %s" %(fromDir, mountDir), shell=True)==0
        else:
            print ("Could not clone Knoppix; mount the filesystem first, please.")
            return False

    def cloneData(self,fromDir):
        """
        Launches the cloning of the persistent data area.
        @param fromDir the directory where lives the model partition contents
        @result True in case of success
       """
        mountDir=self.mountDir()
        result=True
        if mountDir:
            sizeM=Popen(["df", "-B", "M", mountDir], stdout=PIPE).communicate()[0]
            sizeM=sizeM.split("\n")[1]
            sizeM=int(re.split('\s+', sizeM)[3][:-1])
            # create the file for persistent data
            # ##### this is the fast method : don't zero anything #############
            persistentFile=open("%s/KNOPPIX/knoppix-data.img" %mountDir,"w")
            persistentFile.seek(sizeM*1024*1024)
            persistentFile.write(".")
            persistentFile.close()
            # ##### that was the slow method, zeroing all the file ############
            # cmd="dd if=/dev/zero of=%s/KNOPPIX/knoppix-data.img bs=1048576 count=%d;" %(mountDir,sizeM)
            # result=result and call(cmd,shell=True)==0
            ###################################################################

            # format this file as an ext2 filesystem
            mnt1=tempfile.mkdtemp()
            mnt2=tempfile.mkdtemp()
            try:
                # mount the model persistent data on mnt1
                cmd="mount -o loop %s/KNOPPIX/knoppix-data.img %s; " %(fromDir,mnt1)
                result=result and call(cmd, shell=True)==0
                # mount the target persistent data on mnt2
                cmd="mount -o loop %s/KNOPPIX/knoppix-data.img %s; " %(mountDir,mnt2)
                result=result and call(cmd, shell=True)==0
                # clone the persistent data
                cmd="(cd %s; tar cf - .) | (cd %s; tar xf -); " %(mnt1, mnt2)
                result=result and call(cmd, shell=True)==0
            finally:
                cmd="umount %s %s; " %(mnt1, mnt2)
                result=result and call(cmd, shell=True)==0
                cmd="rmdir %s %s)" %(mnt1, mnt2)
                result=result and call(cmd, shell=True)==0
        else:
            result=False
            print ("Could not clone the data; mount the filesystem first, please.")
            return result

    def grubDevice(self):
        """
        @return the grub device name for the usb stick
        """
        cmd="echo | sudo grub-mkdevicemap --device-map=-"
        print (cmd)
        result=Popen(cmd, shell=True, stdout=PIPE).communicate()[0]
        result=result.split("\n")
        expr=re.compile(".*"+self.dev+".*")
        l=[elem for elem in result if expr.match(elem)]
        if len(l)>0:
            return l[0].split("\t")[0]
        else:
            return None

    def grubInstallCmd(self):
        """
        @return the grub install command to make a boot sector in the device
        """
        grubname=self.grubDevice()
        # declose the parentheses, add the zero
        # so the bootable partition is the first one and not the mbr
        grubname=grubname[1:-1]+',0'
        return "grub-install --root-directory=%s '%s'" %(self.mountDir(),grubname)

    def __str__(self):
        result="USB stick (dev=%s); " %self.dev
        result+="total size= %s kB; " %self.sizeK
        result+="vfat size= %s kB; " %self.vfatK
        result+="ext2 size= %s kB; " %self.ext2K
        result+="grub name= %s; " %self.grubDevice()
        result+="parted cmdline= %s; " %self.partedCmd()
        result+="mount point= %s; " %self.mountDir()
        result+="grub install cmdline= %s; " %self.grubInstallCmd()
        return result

if __name__=="__main__":
    import sys
    usbDev=UsbStick(sys.argv[1])
    print (usbDev)


