<style type="text/css">
h1 {
	color: darkred;
}

h2 {
	color: red;
}

h3 {
	color: blue;
}

p, ul, ol {
    margin-left: 30px;
    margin-right: 20px;
}

code, tt {
	font-family: Courier, fixed;
	font-weight: bold;
	color: brown;
}
</style>

# The toolbar ![toolbar](./img/toolbar.png) #

* ![Help button](./img/help.png) The first button on the left of the toolbar provides a window "about" this application, and pulls this document on the top.
* Then, the tools button ![Tools button](./img/gnome-run.png) gives access to maintenance tools for Freeduc disks: to reinitialize them if somebody made them inconsistent, and also backup of persistence data.
* The run button, as it can be guessed, ![Run button](./img/computer.png), allows one to launch the system of the Freeduc disk in a virtual machine (provided the package qemu-kvm is installed).
* The central application is cloning USB Freeduc disks, ![Clone button](./img/jumping-gnu-48x48.png) which can be triggered with the button or by a click one the title nearby.
* The last rightmost button, allows one to quit the application. ![Quit button](./img/application-exit.png) One can interrupt the application even when there is an ongoing command and monitors are displaying its details.

# Details of a cloning action #

In the display just underneath the toolbar, one can see details about all detected USB disks, which are currently plugged to the computer. With GNU-Linux, their identification is ``/dev/sdX`` where ``X`` is some alphabetic character; those characters are generally distributed depending on the plug-in sequence.

When one starts a cloning action ![Clone button](./img/jumping-gnu-48x48.png), one can select multiple disks by checking their boxes; depending on the quality level of the computer, its USB ports, and of the USB sticks, one can have some advantage to clone many of them simultaneously. 

When the disk(s) are selected, one must decide which Freeduc image is to burn into them. When the application is run *from a Freeduc system* the default option is **AUTOCLONE**, which means cloning the USB stick itself onto target USB sticks. One can also select, in either case, an ISO disk image, which must exist somewhere in the file system.

When all the choices are made, the cloning action can begin. It takes some minutes, and one can watch its progress in as many monitors as cloned disks. The longest operation is the copy of the system's core, which will feature booting a Linux kernel, and some hundreds of programs, provided by a compressed read-only filesystem. When this operation is finished, a *persistence* partition is created; this one will contain every modification to the disk during its lifetime: data visible by the user, as well as a load of hidden files, like configurations, ans also every program one wants to install additionally.

When the cloning operation is finished, one can save the monitors' messages, for further use.

# Logics of the maintenance tools #

Unplug a USB stick while the computer is running is easier than doing the same with the hard disk inside it : such a mistake is not uncommon, it can happen to everyone. So, the *persistence* file system may happen to become inconsistent. In most cases, the inconsistent parts are repaired during the next boot. However, the USB stick can become unusable if some inconsistency is really serious.

On the other hand, those USB disks are made to teach computer science on free operating systems, to beginners. Yet *all the permissions* can be granted to the newbie user. If one has got the idea to erase some important file for the computer's features, he can. Fortunately, the core is read-only; when one thinks to erase a core file, one writes actually a code into the persistence, which means "this file is erased"; however the file remains in the core, which is read-only; this explains the magics of the reinitialization.

## Reinitialization of the live USB stick ##

As a matter of fact, to reinitialize the USB stick, it is necessary and sufficient to erase two directories in the persistence partition. Of course, doing so entails the loss all previous work and every configuration made by the user from the begin of the stick's history. That is why maintenance tools are organized in a *cautious* fashion.

## First cautious step: keep the list of additional packages ##

The USB stick comes with hundreds of preinstalled software pieces, but one may want more. Thanks to the software packages of the Debian system, nothing is easier! the ``apt`` commands allow one to do it freely (like in freedom). When the persistence is erased, it is a good idea to keep the list of every additional package, to reinstall them over the core system's packages later.

## Second cautious step: backup user data ##

During the stick history, most of the files which interest the user are inside the directory ``/home/user``; one can make a complete backup thereof, or a selective backup. The tools' interface provides a tree view of this directory, where it is possible to check parts to backup:

* backup everything ... simply check the directory ``user``
* backup selectively, for example contents of the desktop, the downloads, and a project inside the directory ``/home/user/projects/projetc1``: *do not check* the directory ``user``, check the directories ``user/Desktop``, ``user/Downloads``, ``user/projects/project1``; please notice than if two nodes in the file system are checked, the nearest node from the root may *hide* the other one, if it is part of a sub-directory of the previous one: the chosen backup is always the widest one.

The backup is in format ``tar.gz``, also known as ``tgz``; all good archive managers can deal with this format.

If one tries to maintain a new USB stick, there are no personal data, so this step is shortened.

## Last step: reinitialize the persistence ##

This step, as all the previous, can be escaped; one just needs to use the escape button, available everytime. As the reinitialization entails a loss of data, one must confirm twice than one really wants to do it.

This operation is fast, and cannot be reversed. After the reinitialization, the USB stick is ready to behave like at its first boot: so it will boot as slowly as it did during its first usage, because many initializations must be done (a *first* boot is one, two or three minutes longer than the next ones).

# Running the system in a virtual machine #

As long as one is using this application, one can test the restored USB stick in a virtual machine, on the same computer, without stopping it. This is possible if the processor is enabled to support virtualization (which is not the case for all processors).

The ``qemu-kvm`` package is needed to be able to take advantage of this feature. It is not installed by default in the distribution Freeduc, but one can add it. Please notice that working in a virtual machine is one of the seldom means to get screenshots of the system during its boot step or during its shutdown, when the system is enable to manage the screenshot by itself.
