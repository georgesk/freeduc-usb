<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>BackupDialog</name>
    <message>
        <location filename="../ui_selectBackupData.py" line="42"/>
        <source>Select backup data</source>
        <translation type="obsolete">Sélection des données personnelles à enregistrer</translation>
    </message>
    <message>
        <location filename="../ui_selectBackupData.py" line="43"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Please check the tree of data which you want to backup. If you check the topmost node &amp;quot;user&amp;quot;, all and every personal data is chosen, including Mozilla cache and desktop preferences. You may prefer to check subtrees. The subtrees are taken in account only when no node is checked above.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Veuillez cocher le ou les arbres de données que vous voulez sauvegarder. Si on coche le nœud de niveau supérieur «&#xa0;user&#xa0;», toutes les données personnelles sont enregistrées, y compris le cache de Mozilla Firefox et les préférences du bureau. Vous pouvez préférer seulement sélectionner des sous-arbres. Les sous- arbres sont pris en compte si aucun nœud de niveau supérieur n&apos;est coché.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CloneMonitor</name>
    <message>
        <location filename="../monitor.py" line="168"/>
        <source>Cloning is over</source>
        <translation>Le clonage est terminé</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="169"/>
        <source>Cloning</source>
        <translation>Clonage</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../ui_about.py" line="34"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../ui_about.py" line="35"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;© 2019 Georges Khaznadar &amp;lt;georgesk@debian.org&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program is free, it is licensed under GPL-3+.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;© 2019 Georges Khaznadar &amp;lt;georgesk@debian.org&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ce programme est libre, il est régi par la licence GPL-3+.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui_clone_jbart.py" line="124"/>
        <source>Freeduc Clone</source>
        <translation>Freeduc Clone</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="143"/>
        <source>CLONING LIVE STICKS</source>
        <translation>CLONER DES CLÉS USB</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="101"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:16pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="148"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="149"/>
        <source>Tab 2</source>
        <translation>Tab 2</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="118"/>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="119"/>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Aide</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="190"/>
        <source>&amp;Quit</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="191"/>
        <source>&amp;About</source>
        <translation type="obsolete">À &amp;Propos</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="120"/>
        <source>Tools</source>
        <translation type="obsolete">Outils</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="192"/>
        <source>Erase persistence data</source>
        <translation type="obsolete">Effacement des données persistantes</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="193"/>
        <source>When something goes really wrong, erasing persistence data allows one to return to the initial setup</source>
        <translation type="obsolete">Quand ça part vraiment mal, on peut revenir à la configuration initiale en effaçant les données persistantes</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="194"/>
        <source>Backup personal data</source>
        <translation type="obsolete">Enregistrer les données personnelles</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="195"/>
        <source>When persistence data are erase, personal data disappear: you can backup them in advance</source>
        <translation type="obsolete">Quand les données persistantes sont effacées, les données personnelles aussi&#xa0;: on peut les enregistrer par avance</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="196"/>
        <source>List additional packages</source>
        <translation type="obsolete">Lister les paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="197"/>
        <source>When the persistence area is erased all newer package are erased too. One might know their list.</source>
        <translation type="obsolete">Quand on efface la zone de persistance, tous les nouveaux paquets sont effacés aussi. On peut vouloir en connaître la liste.</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="198"/>
        <source>Run in a virtual machine</source>
        <translation type="obsolete">Démarrer dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="169"/>
        <source>Clone the system core to a USB disk</source>
        <translation type="obsolete">Cloner le cœur du système vers un disque USB</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="146"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="171"/>
        <source>Save personal data, Erase persistence</source>
        <translation type="obsolete">Enregistrer les données personnelles, Effacer la persistance</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="173"/>
        <source>Launch in a virtual machine</source>
        <translation type="obsolete">Démarrer dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="177"/>
        <source>Quit the application</source>
        <translation type="obsolete">Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="126"/>
        <source>Help(F1)</source>
        <translation>Aide (F1)</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="128"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="130"/>
        <source>Save personal data, Erase persistence (Alt-S)</source>
        <translation>Enregistrer les données personnelles, Effacer la persistance (Alt-S)</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="132"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="134"/>
        <source>Launch in a virtual machine (Alt-R)</source>
        <translation>Démarrer dans une machine virtuelle (Alt-R)</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="136"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="142"/>
        <source>Clone the system core to a USB disk (Alt-C)</source>
        <translation>Cloner le cœur du système vers un disque USB (Alt-C)</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="140"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="145"/>
        <source>Quit the application (Ctrl-Q)</source>
        <translation>Quitter l&apos;application (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../ui_clone_jbart.py" line="147"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
</context>
<context>
    <name>Monitor</name>
    <message>
        <location filename="../monitor.py" line="63"/>
        <source>stdout</source>
        <translation>Sortie standard</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="65"/>
        <source>stderr</source>
        <translation>Sorties standard des «&#xa0;erreurs&#xa0;»</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="78"/>
        <source>Cloning is over</source>
        <translation type="obsolete">Le clonage est terminé</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="46"/>
        <source>Do you want to save the logs?</source>
        <translation type="obsolete">Voulez-vous enregistrer les traces d&apos;exécution&#xa0;?</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="46"/>
        <source>name of the file for the logs</source>
        <translation type="obsolete">Nom du fichier pour les traces</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="89"/>
        <source>Save the messages?</source>
        <translation>Enregistrer les messages&#xa0;?</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="89"/>
        <source>Log files (*.log);;All files (*)</source>
        <translation>Fichers de journalisation (*.log);;Tous fichiers (*)</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="42"/>
        <source>Close this monitor</source>
        <translation>Fermer ce moniteur</translation>
    </message>
    <message>
        <location filename="../monitor.py" line="45"/>
        <source>Working</source>
        <translation>Travail</translation>
    </message>
</context>
<context>
    <name>MyMain</name>
    <message>
        <location filename="../__init__.py" line="611"/>
        <source>Tab closed.</source>
        <translation>Tab refermé.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="580"/>
        <source>HelpTab.md</source>
        <translation>lang/HelpTab.fr.md</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="588"/>
        <source>Clone ready ... Tab #{0}</source>
        <translation>Clonage terminé ... Tab n°{0}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="648"/>
        <source>Clone to {}</source>
        <translation>Cloner vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="727"/>
        <source>Device</source>
        <translation>Périphérique</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="727"/>
        <source>Partitions</source>
        <translation>Partitions</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="468"/>
        <source>Source</source>
        <translation type="obsolete">Source</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="468"/>
        <source>Actions</source>
        <translation type="obsolete">Actions</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="727"/>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="419"/>
        <source>Own running system</source>
        <translation type="obsolete">Le système en cours</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="504"/>
        <source>Choose an image to clone to {}</source>
        <translation type="obsolete">Choisir une image à cloner vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="368"/>
        <source>ISO Images (*.iso);;All files (*)</source>
        <translation>Images ISO (*.iso);;Tous fichiers (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="513"/>
        <source>Cloning ... Tab #{}</source>
        <translation type="obsolete">Clonage ... Tab n°{}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="776"/>
        <source>Cloning to {}</source>
        <translation>Clonage vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="573"/>
        <source>HelpAbout.md</source>
        <translation>lang/HelpAbout.fr.md</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="706"/>
        <source>Current USB disks: {}</source>
        <translation>Disques USB connectés&#xa0;: {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="708"/>
        <source>No USB stick. Please plug a USB flash disk.</source>
        <translation>Pas de disque USB connecté. Veuillez en connecter un.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="535"/>
        <source>Erase persistence data</source>
        <translation>Effacement des données persistantes</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="536"/>
        <source>If you go forward, all data in the persistence partition(s) will be lost. If unsure, cancel the operation. The list of erasable persistence partitions is below.</source>
        <translation>Si vous persistez, toutes les données dans la ou les partitions de persistace seront effacées. En cas de doute, Échappez à cette opération. La liste des partitions effaçables est ci-dessous.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="543"/>
        <source>Select the partitions to clear</source>
        <translation>Sélection des partitions à effacer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="544"/>
        <source>Be careful. The selected partitions will be cleared when you finish.</source>
        <translation>Attention, les partitions sélectionnées seront effacées quand vous demanderez à finir.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="454"/>
        <source>No selected USB disk</source>
        <translation>Pas de disque USB choisi</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="454"/>
        <source>Please select at least one cell in the disks table.</source>
        <translation>Veuillez sélectionner une case au moins dans le tableau des disques.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="475"/>
        <source>Get a list of additional packages</source>
        <translation>Réupération d&apos;une liste des paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="503"/>
        <source>Select one disk</source>
        <translation>Sélectionner un disque</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="478"/>
        <source>Which disk do you want to check for packages installed in the persistence zone?</source>
        <translation>Sur quel disque voulez-vous vérifier la liste des paquets installés dans la zone de persistance&#xa0;?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="501"/>
        <source>Run a disk in a virtual machine</source>
        <translation>Démarre un disque dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="504"/>
        <source>Which disk do you want to launch in a virtual machine?</source>
        <translation>Quel disque voulez-vous démarrer dans une machine virtuelle&#xa0;?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="151"/>
        <source>ADDITIONAL PACKAGES
===================
</source>
        <translation>LISTE DES PAQUETS AJOUTÉS
=========================
</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="168"/>
        <source>Save Packages File</source>
        <translation>Enregistrement d&apos;un fichier de paquets</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="168"/>
        <source>Markdow files (*.md);;Plain text files (*.txt);;Any file (*)</source>
        <translation>Fichiers Markdow (*.md);;Fichiers texte (*.txt);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="402"/>
        <source>Backup personal data</source>
        <translation>Enregistrer les données personnelles</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="405"/>
        <source>Which is the disk containing personal data to backup?</source>
        <translation>Quel est le disque qui contient les données personnelles à enregistrer&#xa0;?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="139"/>
        <source>List of additional packages in {}</source>
        <translation>Liste des paquets ajoutés dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="320"/>
        <source>Clone a Freeduc system to USB drives</source>
        <translation>Cloner un système Freeduc dans des clés USB</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="322"/>
        <source>Select the target disks</source>
        <translation>Sélection des disques-cibles</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="323"/>
        <source>Please check the disks where you want to clone the system. Warning: all data exiting on those disks will be erased, you can still Escape from this process.</source>
        <translation>Veuillez cocher les disques où vous voulez cloner le système. Attention&#xa0;: toutes les données de ces disques seront effacées. Vous pouvez encore vous échapper de ce processus.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="360"/>
        <source>Source of the core system</source>
        <translation>Source du cœur du système</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="362"/>
        <source>The program is running from a Freeduc GNU-Linux system</source>
        <translation>Le programme fonctionne dans un système GNU Linux Freeduc</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="364"/>
        <source>The program is running from a plain GNU-Linux system</source>
        <translation>Le programme fonctionne dans un système GNU Linux ordinaire</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="368"/>
        <source>Choose an image to clone</source>
        <translation>Choisir une image à cloner</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="115"/>
        <source>Repair and/or manage a Freeduc USB stick</source>
        <translation>Réparer et/ou gérer une clé USB Freeduc</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="118"/>
        <source>Which disk do you want to repair or to save?</source>
        <translation>Quelle clé voulez-vous réparer ou sauvegarder&#xa0;?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="140"/>
        <source>Here is the list of packages which you can restore later. You can edit the text before saving it.</source>
        <translation>Voici la liste des paquets que vous pourriez restaurer plus tard. Il est possible d&apos;éditer ce texte avant de l&apos;enregistrer.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="150"/>
        <source>There are no new packages installed in the persistence partition</source>
        <translation>IL n&apos;y a pas de nouveaux paquets installés sur la partition de persistance</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="286"/>
        <source>Select personal data to save</source>
        <translation>Sélectionnez les données personnelles à enregistrer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="188"/>
        <source>Please check the tree of data which you want to backup. If you check the topmost node &quot;user&quot;, all and every personal data is chosen, including Mozilla cache and desktop preferences. You may prefer to check subtrees. The subtrees are taken in account only when no node is checked above.</source>
        <translation>Veuillez cocher l&apos;arbre de données que vous voulez sauvegarder. Si vous cochez le nœud supérieur «&#xa0;user&#xa0;», toutes les données personnelles sont sélectionnées, y compris le cache de Mozilla Firefox, les préférences de bureau et la (grosse) configuration de l&apos;émulateur Windows. Vous pourriez préférer cocher des sous-arbres. Les sous-arbres sont pris en compte pour autant qu&apos;aucun nœud ne soit coché au-dessus d&apos;eux.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="238"/>
        <source>TAR-GZIP archives (*.tgz,*.tar.gz);;Any file (*)</source>
        <translation>Achives TAR-GZIP (*.tgz,*.tar.gz);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="252"/>
        <source>Save personal persistence data to {}</source>
        <translation>Enregistrer les données personnelles dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="254"/>
        <source>Save to {}</source>
        <translation>Enregistrer dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="270"/>
        <source>save-data</source>
        <translation>donnees</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="270"/>
        <source>Close the log of the archive</source>
        <translation>Fermer le journal d&apos;archivage</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="668"/>
        <source>{} ... Tab #{}</source>
        <translation>{} ... Tab n°&#xa0;{}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="213"/>
        <source>Nothing is available</source>
        <translation>Rien trouvé</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="218"/>
        <source>No personal data in the persistence partition</source>
        <translation>Il n&apos;y a pas de données personnelles dans la partition de persistance</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="287"/>
        <source>Now, you have carefully backuped sensistive data, you may erase the persistence; however, you can still cancel it.</source>
        <translation>Maintenant, vous avez soigneusement sauvegardé les données sensibles, vous pouvez effacer la persistance&#xa0;; mais vous pouvez encore y échapper.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="297"/>
        <source>Persistence erased</source>
        <translation>Données de persistance effacées</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="297"/>
        <source>Persistence data have been erased from {}</source>
        <translation>Les données de persistance de {} ont été effacées</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="304"/>
        <source>Yes, I am sure that I want to erase the persistence.</source>
        <translation>Oui, je suis sûr.e de vouloir effcer la persistence.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="305"/>
        <source>Yes again, I am completely sure!</source>
        <translation>Encore oui, j&apos;en suis complètement sûr.e&#xa0;!</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="238"/>
        <source>Save personal data archive</source>
        <translation>Enregistrer l&apos;archive des données personnelles</translation>
    </message>
</context>
<context>
    <name>PackageEditor</name>
    <message>
        <location filename="../ui_packageEditor.py" line="54"/>
        <source>Additional packages</source>
        <translation>Paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../ui_packageEditor.py" line="55"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This editor contains the packages installed in the persistence zone. They will be lost if one erases the persistence data. You can edit this text and save it.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cet éditeur liste les paquets installés dans la zone de persistance. Ils seront perdus si on efface les données de persistance. Vous pouvez éditer ce texte et l&apos;enregistrer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui_packageEditor.py" line="56"/>
        <source>Cancel</source>
        <translation>Échappement</translation>
    </message>
    <message>
        <location filename="../ui_packageEditor.py" line="57"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
</context>
<context>
    <name>StickMaker</name>
    <message>
        <location filename="../makeLiveStick.py" line="42"/>
        <source>[At {0}]    {1}</source>
        <translation>[À {0}]    {1}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="53"/>
        <source>You must be root to launch {}</source>
        <translation>Il faut être root pour lancer {}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="60"/>
        <source>clone a Freeduc-Jbart distribution</source>
        <translation>cloner une distribution Freeduc-Jbart</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="61"/>
        <source>do&apos;nt use the file rw.tar.gz to seed the persistence</source>
        <translation>ne pas utiliser le fichier rw.tar.gz pour initialiser la partition de persistence</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="62"/>
        <source>source of the distribution (required); the keyword OWN means that we are cloning from Freeduc-Jbart</source>
        <translation>source de la distribution (obligatoire)&#xa0;; le mot-clé OWN signifie qu&apos;on clone depuis une clé USB Freeduc-Jbart</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="63"/>
        <source>the targetted USB stick</source>
        <translation>la clé USB cible</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="73"/>
        <source>Unmounting {}</source>
        <translation>Démontage de {}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="80"/>
        <source>Copying {0} to {1} ...</source>
        <translation>Copie de {0} vers {1} ...</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="113"/>
        <source>Forget the partition {}3, to create it again</source>
        <translation>Suppression de la partition {}3, pour la créer à nouveau</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="120"/>
        <source>Create the partition {}3 to support persistence</source>
        <translation>Création de la partition {}3 pour supporter la persistance</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="121"/>
        <source>This partition is adjusted to use all the remaining space on {}</source>
        <translation>On ajuste cette partition pour utiliser la place restante sur {}</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="156"/>
        <source>Create the file persistence.conf on the third partition</source>
        <translation>Création du fichier persistence.conf sur la troisième partition</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="163"/>
        <source>Pre-seed the third partition with rw.tar.gz</source>
        <translation>Initialisation de la troisième partition à l&apos;aide de rw.tar.gz</translation>
    </message>
    <message>
        <location filename="../makeLiveStick.py" line="182"/>
        <source>Ready. Total time: {}</source>
        <translation>Terminé. Durée totale&#xa0;: {}</translation>
    </message>
</context>
<context>
    <name>Tool</name>
    <message>
        <location filename="../tools.py" line="316"/>
        <source>Cannot erase persistence data</source>
        <translation>Impossible d&apos;effacer les données de persistance</translation>
    </message>
    <message>
        <location filename="../tools.py" line="316"/>
        <source>One needs root priviledge to erase persistence data</source>
        <translation>Il faut avoir les privilèges de super-utilisateur pour effacer les données de persistence</translation>
    </message>
    <message>
        <location filename="../tools.py" line="329"/>
        <source>Persistence erased</source>
        <translation>Données de persistance effacées</translation>
    </message>
    <message>
        <location filename="../tools.py" line="329"/>
        <source>Persistence data have been erased from {}</source>
        <translation>Les données de persistance de {} ont été effacées</translation>
    </message>
    <message>
        <location filename="../tools.py" line="265"/>
        <source>Cancel</source>
        <translation type="obsolete">Échappement</translation>
    </message>
</context>
<context>
    <name>clone_jbart</name>
    <message>
        <location filename="../clone_jbart.py" line="61"/>
        <source>HelpTab.md</source>
        <translation type="obsolete">HelpTab.fr.md</translation>
    </message>
    <message>
        <location filename="../clone_jbart.py" line="74"/>
        <source>Clone ready ... Tab #{0}</source>
        <translation type="obsolete">Clonage terminé ... Tab n°{0}</translation>
    </message>
</context>
<context>
    <name>specialPage</name>
    <message>
        <location filename="../__init__.py" line="134"/>
        <source>Source of the core system</source>
        <translation type="obsolete">Source du cœur du système</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="136"/>
        <source>The program is running from a Freeduc GNU-Linux system</source>
        <translation type="obsolete">Le programme fonctionne dans un système GNU Linux Freeduc</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="138"/>
        <source>The program is running from a plain GNU-Linux system</source>
        <translation type="obsolete">Le programme fonctionne dans un système GNU Linux ordinaire</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="142"/>
        <source>Choose an image to clone</source>
        <translation type="obsolete">Choisir une image à cloner</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="142"/>
        <source>ISO Images (*.iso);;All files (*)</source>
        <translation type="obsolete">Images ISO (*.iso);;Tous fichiers (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="177"/>
        <source>Backup personal data</source>
        <translation type="obsolete">Enregistrer les données personnelles</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="332"/>
        <source>Select one disk</source>
        <translation type="obsolete">Sélectionner un disque</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="180"/>
        <source>Which is the disk containing personal data to backup?</source>
        <translation type="obsolete">Quel est le disque qui contient les données personnelles à enregistrer&#xa0;?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="207"/>
        <source>List of additional packages in {}</source>
        <translation type="obsolete">Liste des paquets ajoutés dans {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="210"/>
        <source>ADDITIONAL PACKAGES
===================
</source>
        <translation type="obsolete">LISTE DES PAQUETS AJOUTÉS
=========================
</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="226"/>
        <source>Save Packages File</source>
        <translation type="obsolete">Enregistrement d&apos;un fichier de paquets</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="226"/>
        <source>Markdow files (*.md);;Plain text files (*.txt);;Any file (*)</source>
        <translation type="obsolete">Fichiers Markdow (*.md);;Fichiers texte (*.txt);;Tout fichier (*)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="283"/>
        <source>No selected USB disk</source>
        <translation type="obsolete">Pas de disque USB choisi</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="283"/>
        <source>Please select at least one cell in the disks table.</source>
        <translation type="obsolete">Veuillez sélectionner une case au moins dans le tableau des disques.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="304"/>
        <source>Get a list of additional packages</source>
        <translation type="obsolete">Réupération d&apos;une liste des paquets ajoutés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="307"/>
        <source>Which disk do you want to check for packages installed in the persistence zone?</source>
        <translation type="obsolete">Sur quel disque voulez-vous vérifier la liste des paquets installés dans la zone de persistance&#xa0;?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="330"/>
        <source>Run a disk in a virtual machine</source>
        <translation type="obsolete">Démarre un disque dans une machine virtuelle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="333"/>
        <source>Which disk do you want to launch in a virtual machine?</source>
        <translation type="obsolete">Quel disque voulez-vous démarrer dans une machine virtuelle&#xa0;?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="363"/>
        <source>Erase persistence data</source>
        <translation type="obsolete">Effacement des données persistantes</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="364"/>
        <source>If you go forward, all data in the persistence partition(s) will be lost. If unsure, cancel the operation. The list of erasable persistence partitions is below.</source>
        <translation type="obsolete">Si vous persistez, toutes les données dans la ou les partitions de persistace seront effacées. En cas de doute, Échappez à cette opération. La liste des partitions effaçables est ci-dessous.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="371"/>
        <source>Select the partitions to clear</source>
        <translation type="obsolete">Sélection des partitions à effacer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="372"/>
        <source>Be careful. The selected partitions will be cleared when you finish.</source>
        <translation type="obsolete">Attention, les partitions sélectionnées seront effacées quand vous demanderez à finir.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="401"/>
        <source>HelpAbout.md</source>
        <translation type="obsolete">lang/HelpAbout.fr.md</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="416"/>
        <source>Clone ready ... Tab #{0}</source>
        <translation type="obsolete">Clonage terminé ... Tab n°{0}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="439"/>
        <source>Tab closed.</source>
        <translation type="obsolete">Tab refermé.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="476"/>
        <source>Clone to {}</source>
        <translation type="obsolete">Cloner vers {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="501"/>
        <source>Cloning ... Tab #{}</source>
        <translation type="obsolete">Clonage ... Tab n°{}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="526"/>
        <source>Current USB disks: {}</source>
        <translation type="obsolete">Disques USB connectés&#xa0;: {}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="528"/>
        <source>No USB stick. Please plug a USB flash disk.</source>
        <translation type="obsolete">Pas de disque USB connecté. Veuillez en connecter un.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="547"/>
        <source>Device</source>
        <translation type="obsolete">Périphérique</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="547"/>
        <source>Partitions</source>
        <translation type="obsolete">Partitions</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="547"/>
        <source>Status</source>
        <translation type="obsolete">Statut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="596"/>
        <source>Cloning to {}</source>
        <translation type="obsolete">Clonage vers {}</translation>
    </message>
</context>
<context>
    <name>uDisk</name>
    <message>
        <location filename="../usbDisk2.py" line="284"/>
        <source>Added partition %s</source>
        <translation>Partition ajoutée&#xa0;: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="300"/>
        <source>Failed to mount the disk: %s</source>
        <translation>Échec au montage du disque&#xa0;: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="343"/>
        <source>Disk not added: not a USB partition</source>
        <translation>Disque non ajouté&#xa0;: partition non-USB</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="307"/>
        <source>Disk not added: empty partition</source>
        <translation>Disque non ajouté&#xa0;: partition vide</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="330"/>
        <source>Already added disk: %s</source>
        <translation>Disque déjà ajouté&#xa0;: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="332"/>
        <source>Added disk: %s</source>
        <translation>Disque ajouté&#xa0;: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="362"/>
        <source>Change for the disk %s</source>
        <translation>Changement pour le disque %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="371"/>
        <source>Disk unplugged from the system: %s</source>
        <translation>Disque débranché du système&#xa0;: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="445"/>
        <source>mount point</source>
        <translation>point de montage</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="446"/>
        <source>size</source>
        <translation>taille</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="447"/>
        <source>brand</source>
        <translation>marque</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="448"/>
        <source>disk model</source>
        <translation>modèle de disque</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="449"/>
        <source>serial number</source>
        <translation>numéro de série</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="452"/>
        <source>check</source>
        <translation>cocher</translation>
    </message>
</context>
</TS>
