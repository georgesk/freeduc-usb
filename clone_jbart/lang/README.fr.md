CLONE_JBART
===========

Cette petite application permet de cloner des clés USB Freeduc-Jbart

Dépendances
-----------

*  **Python3 :** python3-markdown
*  **PyQt5 :** python3-pyqt5
*  **GI :** Gio, GLib, UDisks, gir1.2-udisks-2.0
