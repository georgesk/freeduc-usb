#! /usr/bin/python3

import sys, os
# allow local modules like ressource_rc
sys.path.append(os.path.dirname(__file__))

from PyQt5.QtWidgets import QApplication, QMainWindow,QDialog,\
    QTableWidgetItem,QAbstractScrollArea,QPushButton, QWidget, \
    QFileDialog,QApplication, QWizard, QWizardPage, QVBoxLayout, \
    QFormLayout,QLabel, QCheckBox, QRadioButton, QMessageBox, \
    QLineEdit, QPlainTextEdit, QTreeView
from PyQt5.QtCore import QTranslator, QLocale, Qt, QObject, pyqtSignal
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from ui_clone_jbart import Ui_MainWindow
from ui_about import Ui_Dialog as Ui_AboutDialog
from ui_packageEditor import Ui_PackageEditor
from dbus.mainloop.glib import DBusGMainLoop, threads_init
import gi
gi.require_version('UDisks', '2.0')
import usbDisk2
from tools import Tool, MountPoint, FileTreeSelectorModel
from monitor import Monitor, CloneMonitor
from subprocess import Popen, PIPE
from markdown import markdownFromFile
from datetime import datetime
import io
from subprocess import call

class cloneWizardPage(QWizardPage):
    """
    a custom wizard page
    """
    def __init__(self, parent, ownClone, sourceEdit):
        """
        The constructor
        @param parent a QWidget
        @param ownClone a boolean, True when the active system is Freeduc
        @param sourceEdit a QLineEdit instance which will be defined as
        a wizard field named "source"; this field is touched automatically
        at the initialization of the page, when ownClone is True
        """
        QWizardPage.__init__(self, parent)
        self.ownClone=ownClone
        self.sourceEdit=sourceEdit
        sourceEdit.setReadOnly(True)
        self.registerField("source*", sourceEdit)
        return
    
    def initializePage(self):
        if self.ownClone:
            self.sourceEdit.setText("AUTOCLONE (Freeduc disks can clone themselves natively)")
        return

class myItem(QTableWidgetItem):
    """
    non-editable QTableWidgetItem items
    """
    def __init__(self, text):
        QTableWidgetItem.__init__(self,text)
        self.setFlags(self.flags() & ~Qt.ItemIsEditable)
        return

class MyMain(QMainWindow):

    keysChanged  = pyqtSignal(str) # means that some USB stick has changed
    newmonitor = pyqtSignal(Monitor)
    endmonitor   = pyqtSignal(Monitor)
    closemonitor = pyqtSignal(Monitor)
    
    def __init__(self, lesCles, owndisk, parent=None):
        """
        Main window for the application
        @param lesCles an instance of usbDisk2.Available
        @param owndisk an empty string or something like "sdc"
        @param parent parent window, defaults to None
        """
        QMainWindow.__init__(self, parent)
        self.lesCles=lesCles
        self.owndisk=owndisk
        self.wd=os.path.abspath(os.path.dirname(__file__))
        self.monitors={}
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self)
        self.initTable()
        self.initTab()
        self.initHelpTab()
        
        self.about=QDialog()
        self.about.ui=Ui_AboutDialog()
        self.about.ui.setupUi(self.about)
        self.initAbout()

        self.ui.helpButton.clicked.connect(self.showHelp)
        self.ui.quitButton.clicked.connect(self.close)
        self.ui.toolsButton.clicked.connect(self.toolWizard)
        self.ui.cloneButton.clicked.connect(self.cloneWizard)
        self.ui.cloneButton1.clicked.connect(self.cloneWizard)
        self.ui.runButton.clicked.connect(self.run)
        
        self.keysChanged.connect(self.update_keys)
        self.newmonitor.connect(self.new_monitor)
        self.endmonitor.connect(self.monitor_finished)
        self.closemonitor.connect(self.monitor_closed)
        self.update_keys() # check already connected USB sticks
        return

    def showHelp(self):
        self.about.show()
        self.ui.tabWidget.setCurrentIndex(0) # ensure the help tab is visible
        return

    def rindexFromDev(self, dev):
        """
        @para dev a device
        @return the index of the row of the table widget which contains
        the device, or None if it is not found
        """
        t=self.ui.tableWidget
        rindex=0
        found=False
        for rindex in range(t.rowCount()):
            if t.item(rindex,0).data(0) == dev:
                found=True
                break
        if not found:
            return None
        return rindex
    
    def toolWizard(self, event):
        wiz=QWizard(self)
        wiz.setWindowTitle(self.tr("Repair and/or manage a Freeduc USB stick"))
        page1=QWizardPage(self)
        page1.setTitle(self.tr("Select one disk"))
        page1.setSubTitle(self.tr("Which disk do you want to repair or to save?"))
        target=QLineEdit(page1)
        target.hide()
        page1.registerField("target*", target)
        layout=QVBoxLayout()
        radios={}
        def setTarget(checked):
            for r in radios:
                if r.isChecked():
                    page1.setField("target", r.text())
            return
        for d in self.not_self_keys(format="udev"):
            rb=QRadioButton(d, parent=page1)
            rb.toggled.connect(setTarget)
            layout.addWidget(rb)
            radios[rb]=d
        page1.setLayout(layout)
        wiz.addPage(page1)
        ## assert page1.field("target")
        page2=QWizardPage(wiz)
        disk=wiz.field("target")
        page2.setTitle(self.tr("List of additional packages in {}").format(disk))
        page2.setSubTitle(self.tr("Here is the list of packages which you can restore later. You can edit the text before saving it."))
        layout=QVBoxLayout()
        ed=QPlainTextEdit(page2)
        layout.addWidget(ed)
        newpackages=True
        def page2init(foo=None):
            nonlocal newpackages
            disk=wiz.field("target")
            newpackages=Tool().new_packages(disk)
            if not newpackages:
                packages=self.tr("There are no new packages installed in the persistence partition")
            packages=self.tr("""\
ADDITIONAL PACKAGES
===================
""") + "{}\n{}\n".format(disk,len(disk)*"-") + packages
            c=ed.textCursor()
            c.insertText(packages)
            ed.moveCursor(c.Start)
            ed.ensureCursorVisible()
            return
        page2.setLayout(layout)
        page2.initializePage=page2init
        def page2Validate(foo=None):
            if not newpackages:
                return True
            proposed_fname = "packages-{}.md".format(
                datetime.now().strftime("%Y-%m-%d--%H-%M")
            )
            fname, _ = QFileDialog.getSaveFileName(
                self, self.tr("Save Packages File"),
                proposed_fname,
                self.tr("Markdow files (*.md);;Plain text files (*.txt);;Any file (*)")
            )
            if fname:
                with open(fname,"w") as outfile:
                    outfile.write(ed.toPlainText())
                if "SUDO_UID" in os.environ and "SUDO_GID" in os.environ:
                    ## it is possible to downgrade the owner of the file
                    os.chown(
                        fname,
                        int(os.environ["SUDO_UID"]),
                        int(os.environ["SUDO_GID"])
                    )
            return True
        page2.validatePage=page2Validate
        wiz.addPage(page2)
        page3=QWizardPage(wiz)
        page3.setTitle(self.tr("Select personal data to save"))
        page3.setSubTitle(self.tr('Please check the tree of data which you want to backup. If you check the topmost node "user", all and every personal data is chosen, including Mozilla cache and desktop preferences. You may prefer to check subtrees. The subtrees are taken in account only when no node is checked above.'))
        layout=QVBoxLayout()
        tree=QTreeView(page3)
        layout.addWidget(tree)
        mp=None
        model=None
        exists=False
        def page3Init(foo=None):
            nonlocal mp, model, exists
            mp=MountPoint(wiz.field("target")+"3")
            mp.enter_()
            rootpath=os.path.join(str(mp), "rw", "home")
            exists = os.path.exists(rootpath)
            if exists:
                model = FileTreeSelectorModel(rootpath=rootpath)
                model.setRootPath(rootpath)
                tree.setModel(model)
                tree.setRootIndex(model.parent_index)

                tree.setAnimated(False)
                tree.setIndentation(20)
                tree.setSortingEnabled(True)
            else:
                ## there are no personal data
                model = QStandardItemModel()
                model.setHorizontalHeaderLabels(
                    [self.tr('Nothing is available')])
                tree.setModel(model)
                tree.setUniformRowHeights(True)

                node1 = QStandardItem(
                    self.tr('No personal data in the persistence partition'))
                model.appendRow(node1)
            return
        def page3validate(foo=None):
            nonlocal mp, exists
            if not exists:
                return True
            to_save=[]
            def cb(i, stack):
                if model.checkState(i)==Qt.Checked:
                    pathdata=[index.data() for index in stack+[i]]
                    to_save.append(os.path.join(*pathdata))
                return
            i=model.parent_index
            model.traverseDirectoryWhileUnchecked(i,callback=cb)
            ### ask for a tgz file name and make a tarball into it
            proposed_fname = "DATA-{}.tgz".format(
                datetime.now().strftime("%Y-%m-%d--%H-%M")
            )
            fname=""
            if to_save:
                fname, _ = QFileDialog.getSaveFileName(
                    self, self.tr("Save personal data archive"),
                    proposed_fname,
                    self.tr("TAR-GZIP archives (*.tgz,*.tar.gz);;Any file (*)")
                )
            if fname:
                ### launch the command asynchronously with a Monitor
                command = "/usr/bin/tar"
                args=[
                    "czvf",
                    fname
                ] + [os.path.join(str(mp), "rw", p) for p in to_save]
                ### add a new tab and a monitor
                n=self.ui.tabWidget.count()
                title=self.tr("Save personal persistence data to {}").format(fname)
                newWidget=QWidget()
                self.ui.tabWidget.addTab(
                    QWidget(),
                    self.tr("Save to {}").format(fname)
                )
                rindex=self.rindexFromDev(wiz.field("target"))
                if rindex==None:
                    return True
                # rindex is defined, it bears the row number for the device
                def makeFinishCallback():
                    return lambda: mp.exit_()
                m=Monitor(
                    command, args, self, n, rindex,
                    finishCallback=makeFinishCallback(),
                    environ=os.environ,
                    createfiles=[fname],
                    actionMessage=self.tr("save-data"),
                    buttonMessage=self.tr("Close the log of the archive"),
                )
                self.monitors[m]=wiz.field("target")
                m.start()
            return True
        page3.initializePage=page3Init
        page3.validatePage=page3validate
        page3.setLayout(layout)
        wiz.addPage(page3)
        page4=QWizardPage(wiz)
        page4.setTitle(self.tr("Select personal data to save"))
        page4.setSubTitle(self.tr('Now, you have carefully backuped sensistive data, you may erase the persistence; however, you can still cancel it.'))
        def page4validate(foo=None):
            if wiz.field("ok1") and  wiz.field("ok2"):
                p3=wiz.field("target")+"3"
                call ("umount {}".format(p3), shell=True)
                def page4CallBackMaker():
                    def callback():
                        with MountPoint(wiz.field("target")+"3") as mp:
                            confFile=os.path.join(str(mp),"persistence.conf")
                            call("echo '/ union' > {}".format(confFile), shell=True)
                        QMessageBox.information(
                            self,
                            self.tr("Persistence erased"),
                            self.tr("Persistence data have been erased from {}").format(wiz.field("target")+"3")
                        )
                        return
                    return callback
                command="mkfs"
                args= [
                    "-F",
                    "-L",
                    "persistence",
                    "-t",
                    "ext4",
                    p3
                ]
                n=self.ui.tabWidget.count()
                title=self.tr("Format the partition {}").format(p3)
                newWidget=QWidget()
                self.ui.tabWidget.addTab(
                    QWidget(),
                    self.tr("Format the partition {}").format(p3)
                )
                rindex=self.rindexFromDev(wiz.field("target"))
                if rindex==None:
                    return True                
                m=Monitor(command, args, self, n, rindex,
                          actionMessage="Formating",
                          finishCallback=page4CallBackMaker()
                )
                self.monitors[m]=wiz.field("target")
                m.start()
            return True
        layout=QVBoxLayout()
        request1CB=QCheckBox(self.tr("Yes, I am sure that I want to erase the persistence."))
        request2CB=QCheckBox(self.tr("Yes again, I am completely sure!"))
        page4.registerField("ok1", request1CB)
        page4.registerField("ok2", request2CB)
        layout.addWidget(request1CB)
        layout.addWidget(request2CB)
        page4.setLayout(layout)
        page4.validatePage=page4validate
        wiz.addPage(page4)
        ok=wiz.exec_()
        ##### clean remaining temporary mounts
        
        return
    
    def cloneWizard(self, event):
        wiz=QWizard(self)
        wiz.setWindowTitle(self.tr("Clone a Freeduc system to USB drives"))
        page1=QWizardPage(self)
        page1.setTitle(self.tr("Select the target disks"))
        page1.setSubTitle(self.tr("Please check the disks where you want to clone the system. Warning: all data exiting on those disks will be erased, you can still Escape from this process."))
        selectedDisks=QLineEdit()
        selectedDisks.setReadOnly(True)
        selectedDisks.hide() # not visible, it bears juste a calculated field
        page1.registerField("selectedDisks*", selectedDisks)
        boxes={}
        def updateSelectedDisks(checkState=-1):
            """
            callback for changes in checkboxes
            """
            selected=[d for b,d in boxes.items() \
                      if b.checkState()==Qt.Checked]
            sel=",".join(selected)
            page1.setField("selectedDisks", sel)
            if checkState==-1:
                #called before the wizard is initialized
                selectedDisks.setText(sel)
            return
        layout=QVBoxLayout()
        for disk in self.not_self_keys(format="udev"):
            shortParts=self.lesCles.parts_summary(disk)
            chb=QCheckBox("{} [{}]".format(
                disk,
                ", ".join(shortParts))
            )
            boxes[chb]=disk
            chb.stateChanged.connect(updateSelectedDisks)
            layout.addWidget(chb)
        layout.addWidget(selectedDisks)
        page1.setLayout(layout)
        wiz.addPage(page1)
        sourceEdit=QLineEdit()
        page2=cloneWizardPage(
            self,
            os.path.exists("/usr/lib/live/mount/persistence"),
            sourceEdit
        )
        page2.setTitle(self.tr("Source of the core system"))
        if page2.ownClone:
            page2.setSubTitle(self.tr("The program is running from a Freeduc GNU-Linux system"))
        else:
            page2.setSubTitle(self.tr("The program is running from a plain GNU-Linux system"))
        layout=QVBoxLayout()
        page2.setLayout(layout)
        def chooseISO(event=None):
            source, _ = QFileDialog.getOpenFileName(
                caption = self.tr("Choose an image to clone"),
                filter = self.tr("ISO Images (*.iso);;All files (*)")
            )
            page2.setField("source", source)
            
        chooseButton=QPushButton("Choose the source of the core system")
        chooseButton.clicked.connect(chooseISO)
        layout.addWidget(chooseButton)
        layout.addWidget(sourceEdit)
        wiz.addPage(page2)
        result=wiz.exec_()
        if result:
            source=wiz.field("source")
            disks=wiz.field("selectedDisks").split(",")
            for d in disks:
                self.clonage(source,d)
        return result

    def backup(self):
        """
        Backups the personal data of the currently selected disk
        """
        backup_paths=self.selDisksAndApply(self.backupDialog, Tool().select_backup)
        return

    def backupDialog(self, devices, available):
        """
        create a dialog : select one disk to backup its personal data
        @param paths a list of paths to USB devices
        @param available an Available instance
        """
        disks=sorted(["/dev/"+d for d in devices])
        wiz=QWizard(self)
        wiz.setWindowTitle(self.tr("Backup personal data"))
        page1=QWizardPage(self)
        page1.setTitle(self.tr("Select one disk"))
        page1.setSubTitle(self.tr("Which is the disk containing personal data to backup?"))
        layout=QVBoxLayout()
        radios={}
        first=True
        for d in disks:
            rb=QRadioButton(d, parent=page1)
            if first:
                rb.setChecked(True)
                first=False
            layout.addWidget(rb)
            radios[rb]=d
        wiz.addPage(page1)
        result=wiz.exec_()
        return result, [d for rb, d in radios.items() if rb.isChecked()]

    def run(self):
        """
        Run one drive in a virtual machine provided by qemu-kvm
        """
        devices=self.not_self_keys(format="udev")
        is_ok, disks = self.runDialog(devices, self.lesCles)
        result={}
        if is_ok:
            for d in disks:
                result[d]=Tool().runDisk(d, self)
        return result

    def erase(self):
        """
        Erase the persistence data of the currently selected disks
        """
        self.selDisksAndApply(self.eraseDialog, Tool().erase_persistence)
        return

    def selDisksAndApply(self, wizard, method):
        """
        apply a method to disks selected in the table view
        @param wizard a method with profile (path set, Available -> is_ok, uDisk2 list)
        to let the user select USB disks in a list
        @param method a method with the profile (uDisk2, QWindow -> something)
        which does the job for one uDisk2 instance
        @return a dictionary disk => result of method applied to this disk
        """
        model=self.ui.tableWidget.model()
        sel=self.ui.tableWidget.selectedIndexes()
        devices=set(
            [str(model.data(model.index(s.row(),0))) for s in sel]
        )
        if not devices:
            QMessageBox.critical(
                self,
                self.tr("No selected USB disk"),
                self.tr("Please select at least one cell in the disks table.")
            )
            return
        is_ok, disks = wizard(devices, self.lesCles)
        result={}
        if is_ok:
            for d in disks:
                result[d]=method(d, self)
        return result

    def packageDialog(self, devices, available):
        """
        create a dialog select one disk and get its additional packages
        @param paths a list of paths to USB devices
        @param available an Available instance
        """
        disks=sorted(["/dev/"+d for d in devices])
        wiz=QWizard(self)
        wiz.setWindowTitle(self.tr("Get a list of additional packages"))
        page1=QWizardPage(self)
        page1.setTitle(self.tr("Select one disk"))
        page1.setSubTitle(self.tr("Which disk do you want to check for packages installed in the persistence zone?"))
        layout=QVBoxLayout()
        radios={}
        first=True
        for d in disks:
            rb=QRadioButton(d, parent=page1)
            if first:
                rb.setChecked(True)
                first=False
            layout.addWidget(rb)
            radios[rb]=d
        wiz.addPage(page1)
        result=wiz.exec_()
        return result, [d for rb, d in radios.items() if rb.isChecked()]

    def runDialog(self, devices, available):
        """
        create a dialog to run one disk in a virtual machine
        @param paths a list of paths to USB devices
        @param available an Available instance
        """
        disks=sorted([d for d in devices])
        wiz=QWizard(self)
        wiz.setWindowTitle(self.tr("Run a disk in a virtual machine"))
        page1=QWizardPage(self)
        page1.setTitle(self.tr("Select one disk"))
        page1.setSubTitle(self.tr("Which disk do you want to launch in a virtual machine?"))
        layout=QVBoxLayout()
        radios={}
        first=True
        for d in disks:
            rb=QRadioButton(d, parent=page1)
            if first:
                rb.setChecked(True)
                first=False
            layout.addWidget(rb)
            radios[rb]=d
        page1.setLayout(layout)
        wiz.addPage(page1)
        result=wiz.exec_()
        return result, [d for rb, d in radios.items() if rb.isChecked()]

    def eraseDialog(self, devices, available):
        """
        create a dialog to erase some persistence partitions
        @param paths a list of paths to USB devices
        @param available an Available instance
        """
        persistence_parts=[
            p for d in devices for p in \
            available.parts_ud("/org/freedesktop/UDisks2/block_devices/"+d) \
            if p.label=="persistence"
        ]
        parts=sorted(persistence_parts, key=lambda p: p.devStuff)
        wiz=QWizard(self)
        wiz.setWindowTitle(self.tr("Erase persistence data"))
        page1=QWizardPage(self)
        page1.setTitle(self.tr("Erase persistence data"))
        page1.setSubTitle(self.tr("If you go forward, all data in the persistence partition(s) will be lost. If unsure, cancel the operation. The list of erasable persistence partitions is below."))
        layout=QVBoxLayout()
        for p in parts:
            layout.addWidget(QLabel("{} ({})".format(p.devStuff, p.label)))
        page1.setLayout(layout)
        wiz.addPage(page1)
        page2=QWizardPage(self)
        page2.setTitle(self.tr("Select the partitions to clear"))
        page1.setSubTitle(self.tr("Be careful. The selected partitions will be cleared when you finish."))
        layout=QVBoxLayout()
        boxes={}
        for p in parts:
            cb=QCheckBox("{} ({})".format(p.devStuff, p.label))
            layout.addWidget(cb)
            boxes[cb]=p
        page2.setLayout(layout)
        wiz.addPage(page2)
        result=wiz.exec_()
        return result, [p for b,p in boxes.items() if b.isChecked()]

    def mdToEdit(self, path, edit):
        """
        Feeds a QTextEdit instance with a Markdown file
        @param path the path to the .md file, relative to this file
        @param edit a QTextEdit instance
        """
        path=os.path.join(self.wd, path)
        html=io.BytesIO()
        markdownFromFile(input=path, output=html)
        html.seek(0)
        edit.setHtml(html.read().decode("utf-8"))
        return        

    def initAbout(self):
        """
        Feeds the Dialog with a localized text
        """
        self.mdToEdit(self.tr("HelpAbout.md"), self.about.ui.textEdit)
        return
    
    def initHelpTab(self):
        """
        converts the Markdown help file and writes it into the help tab
        """
        self.mdToEdit(self.tr("HelpTab.md"), self.ui.textEdit)
        return
    
    def monitor_finished(self, monitor):
        """
        callback triggered when a monitor finished its job
        """
        t=self.ui.tableWidget
        t.setItem(monitor.rindex,2,
                  myItem(self.tr("Clone ready ... Tab #{0}").format(monitor.index)))
        t.resizeColumnsToContents()
        return

    def new_monitor(self, monitor):
        """
        callback triggered when a new monitor is ready
        @param monitor a Monitor instance
        """
        self.update_monitored_row(monitor)
        return
        
    def monitor_closed(self, monitor):
        """
        callback triggered when a monitor is closed
        @param monitor a Monitor instance
        """
        device = self.monitors.pop(monitor)
        w=self.ui.tabWidget.widget(monitor.index)
        self.ui.tabWidget.removeTab(monitor.index)
        w.close()
        t=self.ui.tableWidget
        t.setItem(monitor.rindex,2,
                  myItem(self.tr("Tab closed.")))
        t.resizeColumnsToContents()
        return

    def makeRow(self, rindex, disk, already):
        """
        Makes a row in the table view for a disk
        @param index of a row
        @param disk the disk instance
        @param already is True when the first colums is already correct,
        and the row already exists
        @param dic a dictionary disk => partitions
        @return a new value for the index, increased when a row is created
        """

        def description(partition_ud):
            dev=os.path.basename(partition_ud.path)
            label=partition_ud.label
            label1=label.split(" ")[0]
            if len(label) > 11:
                label=label1[:8]+"..."
            if partition_ud.label:
                return "{}({})".format(dev, label)
            else:
                return dev
            
        t=self.ui.tableWidget
        dic=self.lesCles.disksDict()
        nextIndex=rindex
        shortDisk=os.path.join("/dev",os.path.basename(disk))
        shortParts=self.lesCles.parts_summary(disk)
        if not already:
            t.insertRow(rindex)
            t.setItem(rindex,0,myItem(shortDisk))
            nextIndex=rindex+1
            # create the button if it is not already there
            button=QPushButton(self.tr("Clone to {}").format(shortDisk))
            b_function=lambda:self.clonage("/dev/{}".format(shortDisk), button, rindex)
            button.clicked.connect(b_function)
            t.setCellWidget(rindex,3,button)
        t.setItem(rindex,1,myItem(", ".join(shortParts)))
        ## check whether there is still a Monitor with a process for this device
        for m, devicepath in self.monitors.items():
            if shortDisk == os.path.basename(devicepath):
                self.update_monitored_row(m, button)
        return nextIndex

    def update_monitored_row(self, m):
        """
        Some things to run to update a row table if it is already
        related to some monitor
        @param m an instance of Monitor
        """
        t=self.ui.tableWidget
        r=m.rindex
        i=m.index
        t.setItem(
            r, 2,
            myItem(self.tr("{} ... Tab #{}").format(m.actionMessage, i))
        )
        t.resizeColumnsToContents()
        return

    def not_self_keys(self, format=None):
        """
        @param format if this is "udev", will return udev paths
        rather than uDisks2 paths
        @return a list of USB paths, except the own disk which was booted
        as a freeduc system
        """
        if format=="udev":
            return [os.path.join("/dev", os.path.basename(d)) \
                    for d in  sorted(self.lesCles.disksDict()) \
                    if not self.owndisk or self.owndisk not in d]
        # return the uDisks path by default
        return [d for d in  sorted(self.lesCles.disksDict()) \
                if not self.owndisk or self.owndisk not in d]
    
    def update_keys(self):
        """
        callback function used when a USB stick is plugged in or off
        or when its partitins are modified.
        """
        # builds the list of USB sticks, except the stick enventually
        # used to boot the system
        disks=self.not_self_keys()
        t=self.ui.tableWidget
        rindex=0
        ## first, delete rows with disk references which are no longer valid
        for rindex in list(range(t.rowCount()))[::-1]:
            if t.item(rindex,0).data(0) not in disks:
                t.removeRow(rindex)
        ## then, add new disks and update existing ones
        if disks:
            self.statusBar().showMessage(self.tr("Current USB disks: {}").format(", ".join([os.path.basename(d) for d in disks])))
        else:
            self.statusBar().showMessage(self.tr("No USB stick. Please plug a USB flash disk."))
        for disk in disks:
            devDisk=os.path.join("/dev", os.path.basename(disk))
            while rindex < t.rowCount() and \
                  t.item(rindex,0).data(0) < devDisk:
                ## skip the lines before the place of devDisk
                rindex+=1
            already = rindex < t.rowCount() and \
                t.item(rindex,0).data(0) == devDisk
            rindex=self.makeRow(rindex, disk, already)
        t.resizeColumnsToContents()
        return

    def initTable(self):
        """
        initialize the table of devices
        """
        t=self.ui.tableWidget
        t.setColumnCount(3)
        t.setHorizontalHeaderLabels([
            self.tr("Device"), self.tr("Partitions"),self.tr("Status")
        ])
        t.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        return

    def initTab(self):
        """
        initialize the QTabWidget in the user interface
        removes the widget #1, which comes from Designer
        widget #0 is special: it contains some help
        """
        w=self.ui.tabWidget.widget(1)
        self.ui.tabWidget.removeTab(1)
        w.close()
        return

    def clonage(self,source, device):
        """
        Starts cloning
        @param source the path to an ISO file, or an AUTOCLONE statement
        @param device for example /dev/sdd
        """
        t=self.ui.tableWidget
        # find the row index for the line featuring device
        found=False
        for rindex in range(t.rowCount()):
            if t.item(rindex,0).text()==device:
                found=True
                break
        if not found:
            return
        
        assert t.item(rindex,0).text()==device
        ### launch the command asynchronously with a Monitor
        command="/usr/bin/python3"
        if source.startswith("AUTOCLONE"):
            source="own"
        args=[
            "{}/makeLiveStick.py".format(self.wd),
            "--no-persistence",
            "--source",
            source,
            device
        ]
        ### add a new tab and a monitor
        n=self.ui.tabWidget.count()
        title=self.tr("Cloning to {}").format(device)
        newWidget=QWidget()
        self.ui.tabWidget.addTab(
            QWidget(),
            self.tr("Cloning to {}").format(device)
        )
        m=CloneMonitor(command, args, self, n, rindex, environ=os.environ)
        self.monitors[m]=device
        m.start()


def main():
    """
    The main call
    """
    app = QApplication(sys.argv)
    # i18n stuff
    locale = QLocale.system().name()
    translation="clone_jbart_{}.ts".format(locale)
    langPath=os.path.join(os.path.abspath(os.path.dirname(__file__)),"lang",translation)
    translator = QTranslator(app)
    translator.load(langPath)
    app.installTranslator(translator)
    #############
    lesCles=usbDisk2.Available()
    p=Popen("ls /usr/lib/live/mount/persistence", shell=True, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()
    shortDiskNames=out.decode("utf8").split()
    ownDisk=""
    if shortDiskNames:
        # if we have ['sda1', 'sda3'], this should give "sda"
        ownDisk=shortDiskNames[0][:-1]
    w = MyMain(lesCles, ownDisk)
    # addHook is not designed to work with a method inside an object
    # so, let us define the hook outside the main window, and relay
    # a signal to the main window
    # the slot keysChanged in MyMain accepts a string parameter
    # which is the current device if the system is booted from it
    def show_keys(man, obj):
        if lesCles.modified: # filter to detect only USB stick objects
            w.keysChanged.emit(ownDisk)
        lesCles.modified=False
        return
    lesCles.addHook('object-added',   show_keys)
    lesCles.addHook('object-removed', show_keys)
    
    w.show()
    
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
