CLONE_JBART
===========

This little application allows on to clone a freeduc-Jbart USB live stick

Dependances
-----------

*  **Python3:** python3-markdown
*  **PyQt5:** python3-pyqt5
*  **GI:** Gio, GLib, UDisks, gir1.2-udisks-2.0
