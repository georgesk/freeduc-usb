lupdate_only {
  SOURCES +=  $$PWD/__init__.py \
              $$PWD/monitor.py \
              $$PWD/tools.py \
              $$PWD/ui_about.py \
              $$PWD/usbDisk2.py \
              $$PWD/makeLiveStick.py \
              $$PWD/ui_clone_jbart.py \
              $$PWD/ui_packageEditor.py
  }

TRANSLATIONS +=  lang/clone_jbart_fr.ts
