#!/usr/bin/python3

########################################################################
# Clone-KNOPPIX is a utility to clone a Knoppix setup given a file tree
# located in your HDD, onto a removable disk drive, for example a USB
# flash memory stick. This command accepts the following parameters:
########################################################################
# @param Source_Tree the path to the Knoppix structure. It is supposed
#   to contain subdirectories like boot/, efi/, KNOPPIX/.
# @param Vfat_Size it is the size of the vfat partition which will
#   contain the Knoppix setup. The remaining space on the removable
#   drive will be structured in a partition with a reiserfs file system
#   to be used for persistent files. Default unit: MB.
# @param drive is the path to the device file to be fed by Knoppix data.
########################################################################

########################################################################
# © 2013-2016 Georges Khaznadar <georgesk@debian.org>
########################################################################
# Many ideas in this file have been pasted from the file
# flash-knoppix which is distributed with Knoppix media.
# flash-knoppix is © Klaus Knopper 2008-2012
########################################################################
# License: GPL V3
########################################################################

########################################################################
# This utility depends on the following packages:
# - bar to send progress data
# - sfdisk to create the partitions
# - reiserfstools to create the persistence partition
# - dosfstools to create the KNOPPIX partition
# - syslinux to make the boot loader
########################################################################

import sys, subprocess, os, os.path, re, tempfile
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .sequenceApp import sequenceApp

def usage():
    """
    Prints a message to explain the usage of the command
    """
    msg="""\
Usage: {0} <path> <size> <drive>

'path' is the path to the Knoppix structure. It is supposed
       to contain subdirectories like boot/, efi/, KNOPPIX/.

'size' is the size of the vfat partition which will
       contain the Knoppix setup. The remaining space on the removable
       drive will be structured in a partition with a reiserfs file system
       to be used for persistent files. The value is in MB; you can use
       'G' as suffixes to enforce the unit to be gigaBytes.

'drive' is the path to the device file to be fed by Knoppix data.
""".format(sys.argv[0])
    print(msg)
    return

def checkArgs(sourceTree,vfatSize,drive):
    """
    checks the program's arguments
    @param sourceTree is the path to a knoppix file tree
    @param vfatSize is the size wanted for the first partition which
    will contain the knoppix file tree
    @param drive is the path to the device which will be overwritten
    @return an error message which is empty if the check succeeds
    """
    ok=True
    for path in ("KNOPPIX", "boot"):
        ok = ok and os.path.exists(os.path.join(sourceTree,path))
    if not ok:
        return """\
ERROR: the path ``{0}'' does not seem to contain all of the following subdirectories: KNOPPIX, boot.
""".format(sourceTree)
    # now checks the size of the source tree
    size=0
    for dirpath, dirnames, filenames in os.walk(sourceTree):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            size += os.path.getsize(fp)
    sizeM=int(size/1024/1024)
    if int(vfatSize.replace("KiB","")) < sizeM:
        return """\
The size scheduled for the vfat partition ({0} MB) is too low to accomodate the size of the data to write ({1} MB). Please ask for a bigger size.
""".format(vfatSize,sizeM)
    info=device_info(drive)
    if not info:
        return """\
The device {0} is not connected to the computer, please check the setup.
""".format(drive)
    elif info["interface"]!="usb":
        return """\
The device {0} is not connected by USB, please check the setup.
""".format(drive)
    elif info["ATA SMART"]!="not available":
        return """\
The device {0} is probably some hard disk, please check the setup.
""".format(drive)
    size=int(info["size"])
    sizeForReiser=size-int(vfatSize.replace("KiB",""))*1024
    if sizeForReiser < 1.5*1024*1024*1024:
        return """\
The device {0} will have too few space to bear a 1.5 GiB partition for
KNOPPIX-DATA, please check the setup.
""".format(drive)
    ###############################################
    # at this point, everything is ok.
    ###############################################
    return ""

def device_info(drive):
    """
    gets an information string about a drive.
    @param drive the unix path to a drive
    @return a short identifier string, or False upon error
    """
    (out,err) = subprocess.Popen("udisks --show-info {}".format(drive), 
                                 stdout=subprocess.PIPE,
                                 shell=True).communicate()
    pattern=re.compile(r" *(\S[^:\n]*): *([^\n]+)")
    keyVals=re.finditer(pattern,out.decode("utf-8"))
    dico={}
    for kv in keyVals:
        dico[kv.group(1)]=kv.group(2).strip()
    cannot=[k for k in dico if k.startswith("Cannot stat device file")]
    if cannot:
        result=False
    else:
        result=dico
    return result

def normalizeFsize(vfatSize):
    """
    normalize the size argument. It can be vritten with a decimal dot,
    and contain a multiplier like M or G
    @param vfatSize the parameter for afile size
    @return an string value with suffix KiB.
    """
    result=vfatSize
    pattern=r"([.0-9]+)([mMgG])"
    m=re.compile(pattern).match(vfatSize)
    if m:
        if m.group(2) in "mM":
            result=int(1024*float(m.group(1)))
        elif m.group(2) in "gG":
            result=int(1024*1024*float(m.group(1)))
    return "{0}KiB".format(result)

def parseInput():
    """
    make a few checks on the input parameters
    @return a path, an integer and a device path if it successfully checks
    the parameters.
    """
    if len(sys.argv) <= 3:
        usage()
        sys.exit(1)
    sourceTree=sys.argv[1]
    vfatSize=sys.argv[2]
    drive=sys.argv[3]
    vfatSize=normalizeFsize(vfatSize)
    error = checkArgs(sourceTree,vfatSize,drive)
    return error, sourceTree, vfatSize, drive

def umountDrive(drive):
    """
    umounts every mounted partition of a drive
    @parm drive its unix path
    """
    (out,err) = subprocess.Popen("mount | grep {}".format(drive), 
                                 stdout=subprocess.PIPE,
                                 shell=True).communicate()
    pattern=r"/dev/.* on (\S*).*"
    r=re.compile(pattern)
    mPoints=[r.match(l) for l in out.decode("utf-8").split("\n")]
    mPoints=[m.group(1) for m in mPoints if m]
    for m in mPoints:
        subprocess.call("umount {0}".format(m), shell=True)

def run():
    error, sourceTree, vfatSize, drive = parseInput()
    if error:
        print(error)
        sys.exit(1)
    if os.getuid() > 0:
        print ("You must be root to lauch clone-knoppix")
        sys.exit(1)
    ############# everything seems OK, going further ################
    drive_dict=device_info(drive)
    umountDrive(drive)

    app = sequenceApp(sys.argv)

    # zeroes any previous boot structure
    # on the first megabyte of the drive
    cmd="""\
dd if=/dev/zero of="{0}" bs=1024 count=1024
sync
""".format(drive)
    app.appendBash(cmd, "Zeroing boot sectors", duration=1)

    # create two partitions on the drive
    # the following process should output 32 lines
    cmd="""\
sfdisk "{0}" <<EOT
-,{1},0c,*
-,-,L,-
write
y
EOT
""".format(drive, vfatSize)
    app.appendBash(cmd, "Creation of two partitions (vfat and reiserfs)", duration=2, lines=32)

    cmd="""\
echo "Waiting some time to settle the new partitions."
blockdev --flushbufs "{0}"
sleep 5
udevadm settle --timeout=10
""".format(drive)
    app.appendBash(cmd, "Waiting a few seconds to flush buffers", duration=10)

    cmd="""\
ionice mkdosfs -F32 -n "KNOPPIX" "{0}1"
ionice blockdev --flushbufs "{0}1"

ionice mkreiserfs -s 513 -f -f -l "KNOPPIX-DATA" "{0}2"
ionice blockdev --flushbufs "{0}2"
""".format(drive)
    app.appendBash(cmd, "making filesystems", duration=30)

    ## create a temporary directory
    tmpMount=tempfile.mkdtemp(suffix=".tmp", prefix="clone-knoppix")
    cmd="""\
mount -t vfat {0}1 {1}
""".format(drive, tmpMount)
    app.appendBash(cmd, "mounting the vfat partition", duration=1)

    ## compute the size of the data to copy
    size=0
    for dirpath, dirnames, filenames in os.walk(sourceTree):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            size += os.path.getsize(fp)
    sizeM=int(size/1024/1024)
    cmd="""\
ionice tar cf - -C "{0}" . | bar --display-numeric --size "{1}M" | ionice tar xf - -C "{2}"
echo "2 /KNOPPIX-DATA reiserfs" > "{2}/KNOPPIX/knoppix-data.inf"
blockdev --flushbufs "{3}1"
sleep 2
""".format(sourceTree,sizeM,tmpMount,drive)
    app.appendBash(cmd, "copying the knoppix tree to the vfat partition", duration=900, percentRe=r"^([0-9]+)$")

    cmd="""\
umount "{0}1"
rmdir "{1}"
""".format(drive,tmpMount)
    app.appendBash(cmd, "Unmount the vfat partition", duration=1)

    cmd="""\
dd if=/usr/lib/SYSLINUX/mbr.bin of="{0}"
sleep 1
syslinux --directory /boot/syslinux "{0}1"
echo "Installed syslinux"
sleep 2
partprobe -s "{0}"
""".format(drive)
    app.appendBash(cmd, "Takes care of boot sectors", duration=4)

    cmd="""\
echo "Done. You can close the application and unplug the flash drive."
"""
    app.appendBash(cmd, "End of Clone-knoppix.", duration=2)

    sys.exit(app.exec_())
    

def test():
    """
    This routine is there only to test the layout properties
    """
    app = sequenceApp(sys.argv)
    app.appendBash("sleep 1; echo '25%'; sleep 1; echo '50%'; sleep 1; echo '75%'; sleep 1; echo '100%';",
                   "sleeping four seconds",
                   duration=1,
                   percentRe=re.compile(r"(\d+)%")
    )
    app.appendBash("unknown-command", "unknown command: something should appear in the Standard Error tab, take a look at it.", duration=1)
    sys.exit(app.exec_())

if __name__=="__main__":
    test()
