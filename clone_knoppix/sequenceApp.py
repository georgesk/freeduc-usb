"""
@module sequenceApp
implements a particular qt5 Application which allows to
manage sequencial procedure lists coupled with a monitor : 
informations about the pending procedure, progress bar, and
text browsers for the stdout and stderr streams.
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from .processTimer import processTimer
from .processes import bashProcess
from .Ui_cloneDialog import Ui_Dialog
import time, datetime, re

def toHMS(t):
    """
    convert a duration in seconds to the format H:MM:SS
    @param t time in seconds
    @return a string, format H:MM:SS
    """
    h=t//3600
    t-=3600*h
    m=t//60
    t-=60*m
    s=int(t)
    return "%d:%02d:%02d" %(h,m,s)
    

class sequenceApp(QApplication):
    """
    implements a particular qt4 Application which allows to
    manage sequencial procedure lists coupled with a monitor : 
    informations about the pending procedure, progress bar, and
    text browsers for the stdout and stderr streams.
    """

    """
    specific signal which will be triggered by the processTimer object
    """
    idleTick          = pyqtSignal()
    idleDetailProgress = pyqtSignal(int)
    processFinished = pyqtSignal(str)

    def __init__(self, argv):
        """
        the constructor.
        creates specific properties: a specific time (self.t) which
        maintains and manages a list of processes to be run sequentially
        and an index on this list (self.pending). The list of processes
        exists at the application's level (self.pList), the timer keeps a
        reference to it.
        A dialog window is shown, which embeds all the widgets fed by the
        timer and its process list.
        @param argv alist of arguments
        """
        QApplication.__init__(self,argv)
        self.pending=0
        self.pList=[]    ### list of processes(currently only bashProcess class)
        self.t=processTimer(self)
        w = QDialog()
        w.ui=Ui_Dialog()
        w.ui.setupUi(w)
        w.ui.closeButton.clicked.connect(self.stopAndQuit)
        w.ui.reRunButton.clicked.connect(self.start)
        w.show()
        self.dialog=w
        self.idleTick.connect(self.tick)
        self.idleDetailProgress.connect(self.setDetailProgress)
        self.processFinished.connect(self.pFinished)
        self.detailProgress=0 ### detailed index of progress (0 to 100)
        self.timestamps=[]
        self.timestamp()
        self.startTime=0
        return

    def pFinished(self, cmd):
        """
        callback triggered when a process is finished
        @param cmd data from the finished bashProcess
        """
        self.pending+=1
        self.timestamp(cmd)

    def start(self):
        """
        initialize self.pending so any ready bashProcess will be run
        """
        self.t.start(1000)
        self.pending=0
        self.dialog.ui.closeButton.setText("Close")
        self.dialog.ui.reRunButton.setEnabled(False)
        self.startTime=time.time()
        return

    def stopAndQuit(self, event):
        """
        Stops the current process if any, then quits
        """
        pp = self.pendingProcess()
        if pp and pp.state()==QProcess.Running:
            pp.close()
        self.quit()

    def timestamp(self, cmd=""):
        """
        appends a timestamped command to the list of commands which the
        sequenceApp has managed.
        @param cmd a string defining the command just finished
        """
        self.timestamps.append((datetime.datetime.now(),cmd))
        return

    def sofar(self):
        """
        @return the duration in seconds since the previous self.start()
        """
        return time.time()-self.startTime

    def pendingProcess(self):
        """
        get the pendingprocess in self.pList
        @return the pending process if any, else NOne
        """
        if self.pending >= len(self.pList):
            return None
        else:
            return self.pList[self.pending]

    def tick(self):
        """
        callback procedure triggered by processTimer's ticks
        """
        pp = self.pendingProcess()
        if pp and pp.state()==QProcess.NotRunning:
            pp.start()
            self.dialog.ui.stageEdit.setText("{}, for {}".format(pp.message, toHMS(pp.sofar())))
            self.dialog.ui.elapsedEdit.setText(toHMS(self.sofar()))
            self.setDetailProgress(0)
            self.setProgress()
        elif self.pending < len(self.pList):
            self.dialog.ui.stageEdit.setText("{}, for {}".format(pp.message, toHMS(pp.sofar())))            
            self.dialog.ui.elapsedEdit.setText(toHMS(self.sofar()))
        else:
            # no more processes to run
            self.bailOut()

    def setDetailProgress(self, dp):
        """
        callback procedure to update the progress bar
        @param dp the detailed progress index: integer in the range (0,100).
        Used to set the progress bar's cursor between the lower and the
        upper limit of the pending process.
        """
        self.detailProgress=dp
        if self.detailProgress > 100:
            self.detailProgress = 100
        if self.detailProgress < 0:
            self.detailProgress = 0
        self.setProgress()

    def setProgress(self):
        """
        sets the progress barposition by taking in account self.pList
        """
        list_before = self.pList[:self.pending]
        list_included = self.pList[:self.pending+1]
        tb = sum([p.duration for p in list_before])
        ti = sum([p.duration for p in list_included])
        tt = sum([p.duration for p in self.pList])
        progress=(tb+self.detailProgress/100*(ti-tb))/tt
        self.dialog.ui.progressBar.setValue(int(100*progress))
        return


    def appendBash(self, cmd, message, duration=1, lines=None, percentRe=None):
        """
        appends a bash command line to self.pList
        @param cmd a bash commandline
        @param message a message to display as a stage identifier
        @param duration an estimate of the duration of the process in seconds.
        It is used to define alistof expected durations for the progress bar.
        @param lines if not None, it is the number of expected lines from
        stdout and stderr streams. It is taken in account to manage the
        progress bar when the number of output lines matters.
        @param percentRe a regular expression which can be used to filter
        an integer from the stdout stream. It is taken in account to
        manage the progress bar when not None.
        """
        self.pList.append(bashProcess(self, cmd, message, duration, lines, percentRe))
        return

    def exec_(self):
        """
        launches the processTimer before entering the main loop
        """
        self.start()
        return QApplication.exec_()

    def bailOut(self):
        """
        Notifies the end of the sequence of processes, and gives the
        user an opportunity to close the application.
        """
        self.dialog.ui.stageEdit.setText("All processes are finished. Duration = " + toHMS(self.sofar()))
        self.dialog.ui.progressBar.setValue(100)
        self.dialog.ui.closeButton.setText("Close the window")
        self.dialog.ui.reRunButton.setEnabled(True)
        self.t.stop()
        return



if __name__=="__main__":
    print("no test for sequenceApp")
