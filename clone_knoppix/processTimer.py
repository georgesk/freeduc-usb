from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class processTimer(QTimer):
    """
    This class launches a process defined in a sequenceApp at each
    opportunity
    """
    def __init__(self, parent):
        """
        the constructor.
        @param parent a sequenceApp which maintains the process list and index
        """
        QTimer.__init__(self, parent)
        self.timeout.connect(self.idleProcess)

    def idleProcess(self):
        """
        callback for the timer.Starts a process in the list if
        one of them is to be started. Otherwise the application
        is closed, after a warning message.
        """
        self.parent().idleTick.emit()
            
