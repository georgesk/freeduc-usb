"""
definition of processes which can interact with a sequenceApplication
"""

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import re, time

class bashProcess(QProcess):
    """
    class to implement abstract processes to be run in a sequence by
    a sequenceApplication
    """
    defaultCmd="""\
echo "This is the default bashProcess command, it should be redefined."
"""
    def __init__(self, sqApp, cmd = None, message = None,
                 duration=1, lines=None, percentRe=None):
        """
        Constructor.
        @parameter sqApp a sequenceApplication to hook to
        @param cmd a command line which will be interpreted by bash
        @param message a string to summarize the command lines
        @param duration an estimate of the duration of the process in seconds.
        It is used to define alistof expected durations for the progress bar.
        @param lines if notNOne, it is the number of expected lines from
        stdout and stderr streams. It is taken in account to manage the
        progress bar when the number of output lines matters.
        @param percentRe a regular expression which can be used to filter
        an integer from the stdout stream. It is taken in account to
        manage the progress bar when not None.
        """
        QProcess.__init__(self, parent=None)
        self.sqApp = sqApp
        if message:
            self.message=message
        else:
            self.message="no message"
        self.outBrowser=self.sqApp.dialog.ui.stdoutBrowser
        self.errBrowser=self.sqApp.dialog.ui.stderrBrowser
        self.startTime=0
        self.duration=duration
        self.lines=lines
        self.percentRe=percentRe
        if self.percentRe:
            self.regexp=re.compile(self.percentRe)
        else:
            self.regexp=None
        if cmd:
            self.cmd=cmd
        else:
            self.cmd=bashProcess.defaultCmd
        self.readyReadStandardOutput.connect(self.stdout)
        self.readyReadStandardError.connect(self.stderr)
        self.finished.connect(self.finish)
        return

    def start(self):
        self.outBrowser.append(inFrame(self.message))
        self.startTime=time.time()
        return QProcess.start(self,"bash", ["-c", self.cmd])

    def sofar(self):
        return time.time()-self.startTime
        
    def stdout(self):
        stream=bytes(self.readAllStandardOutput()).decode("utf-8")
        if not self.percentRe:
            self.outBrowser.append(stream)
        else:
            numbers=self.regexp.findall(stream)
            if len(numbers)==0:
                return
            n=int(numbers[-1])
            if 0 <= n <= 100:
                self.sqApp.idleDetailProgress.emit(n)
        return

    def stderr(self):
        stream=bytes(self.readAllStandardError()).decode("utf-8")
        if not self.percentRe:
            self.errBrowser.append(stream)
        else:
            numbers=self.regexp.findall(stream)
            if len(numbers)==0:
                return
            n=int(numbers[-1])
            if 0 <= n <= 100:
                self.sqApp.idleDetailProgress.emit(n)
        return

    def finish(self):
        self.sqApp.processFinished.emit(self.cmd)
        return

def inFrame(msg, maxl=72, margin=4):
    """
    creates a surrounding frame around a message
    @param msg the input message
    @param maxl max length occupied by words in a line; defaults to 72
    @param margin total spaces used for margins, defaults to 4
    @return a shell command to echo this message with a surrounding frame
    """
    words=msg.split()
    i=0
    lines=[]
    # gathers words in lines no longer than maxl
    l=""
    while i < len(words):
        if len(l)+len(words[i])<maxl:
            l=l+" "+words[i]
        else:
            lines.append(l)
            l=""
            l=l+" "+words[i]
        i+=1
    lines.append(l)
    for i in range(len(lines)):
        lmargin=(margin+maxl-len(lines[i]))//2
        rmargin=margin+maxl-len(lines[i])-lmargin
        lines[i]="|"+" "*lmargin+lines[i]+" "*rmargin+"|"
    formattedMsg="+"+"-"*(margin+maxl)+"+"
    for l in lines:
        formattedMsg+="\n"+l
    formattedMsg+="\n+"+"-"*(margin+maxl)+"+"

    return formattedMsg
