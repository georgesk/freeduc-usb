DESTDIR =

INTERFACES = $(patsubst %.ui, Ui_%.py, $(wildcard *.ui))
MANPAGES = $(patsubst %.xml, %.1, $(wildcard bin/*.xml))

all: $(INTERFACES)

manpages:  $(MANPAGES)

Ui_%.py : %.ui
	@echo -n "Compiling interface $< ===>  $@ ..."
	@pyuic5 $< > $@
	@echo "Done."

PYTHONDIR = $(DESTDIR)/usr/share/pyshared/freeduc_usb
SHAREDIR  = /usr/share/python-freeduc-usb

install: all

clean:
	rm -rf $(INTERFACES) *~ __pycache__ freeduc_utils/*~ freeduc_utils/__pycache__

DB2MAN = /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/manpages/docbook.xsl
XP     = xsltproc --nonet --param man.charmap.use.subset "0"

%.1: %.xml
	cd $$(dirname $<); $(XP) $(DB2MAN) $$(basename $<)


.PHONY: all install clean

